package com.ebms.portal.automation.builder.components;

import com.ebms.portal.automation.builder.factory.FactoryMethods;
import com.codeborne.selenide.SelenideElement;
import lombok.Getter;
import lombok.Setter;

public class BasePageComponent<T> {
    @Getter @Setter private boolean isInitializedWithParent = false;
    @Setter private SelenideElement title;
    @Getter @Setter private FactoryMethods methods;
    @Getter private T object;

    @SuppressWarnings("unchecked")
    public void setObject(BasePageComponent<?> requester) {
        this.object = (T) requester;
    }

    public String getComponentTitle() {
        return title.getText();
    }
}
