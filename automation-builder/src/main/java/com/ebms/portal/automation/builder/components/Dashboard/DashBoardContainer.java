//package com.ebms.portal.automation.builder.components.Dashboard;
//
//import com.ebms.portal.automation.builder.components.DataContainer;
//import com.ebms.portal.automation.builder.factory.annotation.ObjectLoaderMeta;
//import com.ebms.portal.automation.builder.factory.annotation.Platform;
//import com.codeborne.selenide.Condition;
//import com.codeborne.selenide.ElementsCollection;
//import com.codeborne.selenide.Selenide;
//import com.codeborne.selenide.SelenideElement;
//import lombok.Getter;
//
//import javax.xml.crypto.Data;
//import java.util.*;
//@Getter
//public class DashBoardContainer {
//
//    ElementsCollection infoCells;
//    SelenideElement title;
//    SelenideElement value;
//    ElementsCollection memberName;
//    ElementsCollection memberValue;
//    ElementsCollection viewCardDetailsTabs;
//    final Map<String, SelenideElement> viewCardDetailsProductTab = new LinkedHashMap<>();
//    SelenideElement printViewCard;
//    SelenideElement downloadViewCard;
//    ElementsCollection dashboardUsers;
//    SelenideElement rightArrow;
//    SelenideElement leftArrow;
//    Map<String, String> info = new HashMap<>();
//
//    public DashBoardContainer(SelenideElement parent) {
//        parent.scrollIntoView(true);
//        title = parent.$("[data-text='title']");
//        memberName = parent.$$("[class^='ui-member-info-and-listing-widget__memberInfo'] span[class$='title']");
//        memberValue = parent.$$("[class^='ui-member-info-and-listing-widget__memberInfo'] span[class$='value']");
//        viewCardDetailsTabs = parent.$$("[data-action-name ='button']");
//        for(SelenideElement tab : viewCardDetailsTabs) {
//            viewCardDetailsProductTab.put(tab.getText(), tab);
//        }
//        downloadViewCard = parent.$("[data-action-name ='download']");
//        printViewCard = parent.$("[data-action-name ='print']");
//        dashboardUsers = parent.$$("[class$='cards__card__name']");
//        rightArrow = parent.$("[class*='next ui-force-scroller__filter']");
//        leftArrow = parent.$("[class*='nav-button--prev ui-force-scroller']");
//        this.infoCells = parent.$$("[class$='planDetails__details']");
//    }
//
//    public String getTitle() {
//        return title.getText();
//    }
//
//    public List<String> getViewCardTabs() {
//        List<String> viewCardList = new ArrayList<>();
//        for (int i = 0; i <= 1; i++) {
//            viewCardList.add(viewCardDetailsTabs.get(i).getText());
//        }
//        return viewCardList;
//    }
//
//    public void getViewCardTabClickDental() {
//        for (int i = 0; i < 1; i++) {
//            viewCardDetailsTabs.get(i).click();
//        }
//    }
//
//    public List<String> getMemberUserName() {
//        int size = dashboardUsers.size();
//        List<String> members = new ArrayList<>();
//        for (int i = 0; i < size; i++) {
//            members.add(dashboardUsers.get(i).getText());
////            dashboardUsers.get(i).click();
//        }
//        return members;
//    }
//
//    public void selectMember(String member) {
//        int size = dashboardUsers.size();
//        for (int i = 0; i < size; i++) {
//          if (dashboardUsers.get(i).getText().equals(member))
//            dashboardUsers.get(i).click();
//        }
//    }
//
//    public void clickOnRightArrow(){
//        while(rightArrow.isDisplayed()) {
//            rightArrow.click();
//        }
//    }
//
//    public void clickOnLeftArrow(){
//        while (leftArrow.isDisplayed()) {
//            leftArrow.click();
//        }
//    }
//
////    public void getViewCardPrint()
////    {
////        printViewCard.click();
////    }
//
//    public void getViewCardDownload() {
//        downloadViewCard.click();
//    }
//
//    public void getViewCardTabClickVision() {
//        for (int i = 1; i < 2; i++) {
//            viewCardDetailsTabs.get(i).click();
//        }
//    }
//
//    public void clickProductTab(String product) {
//        viewCardDetailsProductTab.get(product).click();
//        Selenide.$("[class$='preloader']").should(Condition.appear);
//        Selenide.$("[class$='preloader']").should(Condition.disappear);
//    }
//
//    public Map<String, String> getPlanDetails() {
//        for (SelenideElement infoCell : infoCells) {
//            DataCell cell = new DataCell(infoCell);
//            info.put(cell.getTitle(), cell.getValue());
//        }
//        return info;
//    }
//
//    public Map<String, String> getMemberDetails() {
//        Map<String, String> memberNameDetails = new LinkedHashMap<>();
//        for (int i = 0; i <= 1; i++) {
//            memberNameDetails.put(memberName.get(i).getText(), memberValue.get(i).getText());
//        }
//        return memberNameDetails;
//    }
//
//    public List<Map<String, String>> getMemberDetailsValues() {
//        List<Map<String, String>> memberList = new ArrayList<>();
//        memberList.add(getMemberDetails());
//        return memberList;
//    }
//
//    public List<Map<String, String>> getPlanDetailsValues() {
//        List<Map<String, String>> list = new ArrayList<>();
//        list.add(getPlanDetails());
//        return list;
//    }
//
//
//    @Getter
//    public static class DataCell {
//        String title;
//        String value;
//
//        public DataCell(SelenideElement parent) {
//            title = parent.$("[class$='planDetails__details--label']").getText();
//            value = parent.$("[class$='planDetails__details--value']").getText();
//        }
//    }
//
//    @ObjectLoaderMeta(platform = Platform.WEB)
//    public DashBoardContainer loadWeb() {
//        return this;
//    }
//}
