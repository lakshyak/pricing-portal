package com.ebms.portal.automation.builder.components;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

public class DataContainer {

    ElementsCollection infoCells;
    SelenideElement title;
    Map<String, String> info = new HashMap<>();

    public DataContainer(SelenideElement parent) {
        parent.scrollIntoView(true);
        title = parent.$("[data-text='title']");
        this.infoCells = parent.$$(".ui-grid__item");
    }

    public String getTitle() {
        return title.getText();
    }

    public Map<String, String> getFieldsWithValues() {
        for (SelenideElement infoCell : infoCells) {
            DataCell cell = new DataCell(infoCell);
            info.put(cell.getTitle(), cell.getValue());
        }
        return info;
    }

    public String getFieldValue(String fieldName) {
        return info.get(fieldName);
    }

    @Getter
    public static class DataCell {
        String title;
        String value;

        public DataCell(SelenideElement parent) {
            title = parent.$(".ui-grid__item__label").getText();
            value = parent.$(".ui-grid__item__value--data").getText();
        }
    }
}



