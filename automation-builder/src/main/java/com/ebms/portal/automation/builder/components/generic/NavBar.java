package com.ebms.portal.automation.builder.components.generic;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

public class NavBar {

    @Getter
    SelenideElement parent;

    @Getter
    Map<String, SelenideElement> links = new HashMap<>();

    public NavBar(SelenideElement parent) {
        this.parent = parent;
        ElementsCollection navigationLinks = parent.$$("a");
        for (SelenideElement link : navigationLinks) {
            if (!link.getText().equals("")) links.put(link.getText(), link);
        }
    }

    public void navigateTo(String link) {
        links.get(link).click();
    }

}
