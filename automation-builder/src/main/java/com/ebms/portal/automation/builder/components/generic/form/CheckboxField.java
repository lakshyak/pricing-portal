package com.ebms.portal.automation.builder.components.generic.form;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import lombok.Getter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CheckboxField implements FormElement {

    @Getter SelenideElement parent;
    @Getter SelenideElement label;
    Map<String, SelenideElement> options = new HashMap<>();
    private static final String ATTRIBUTE_OPTION_SELECTED = "data-selected";

    public CheckboxField(SelenideElement parent) {
        this.parent = parent;
        parent.shouldBe(Condition.visible).scrollIntoView(false);
        label = this.parent.$("*[data-text='label']");
        ElementsCollection collection = parent.$$("*[data-option]");
        collection.forEach(i -> options.put(i.getText().trim(), i));
    }

    @Override
    public void setValue(String... values) {
        Arrays.stream(values).forEach(v -> options.computeIfPresent(v, (key, val) -> {
            if (val.getAttribute(ATTRIBUTE_OPTION_SELECTED).equalsIgnoreCase("false")) val.click();
            return val;
        }));
    }

    @Override
    public void clear() {
        options.forEach( (k,v) -> {
            if (v.getAttribute(ATTRIBUTE_OPTION_SELECTED).equalsIgnoreCase("true")) v.click();
        });
    }

    @Override
    public void removeValue(String... values) {
        Arrays.stream(values).forEach(v -> options.computeIfPresent(v, (key, val) -> {
            if (val.getAttribute(ATTRIBUTE_OPTION_SELECTED).equalsIgnoreCase("true")) val.click();
            return val;
        }));
    }

    @Override
    public List<String> getValue() {
        return options.values().stream()
                .filter(element -> element.getAttribute(ATTRIBUTE_OPTION_SELECTED).equalsIgnoreCase("true"))
                .map(SelenideElement::getText)
                .collect(Collectors.toList());
    }
}
