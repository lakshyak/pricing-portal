package com.ebms.portal.automation.builder.components.generic.form;

import com.codeborne.selenide.SelenideElement;

import java.util.List;

public interface FormElement {

    SelenideElement getParent();

    SelenideElement getLabel();

    void setValue(String[] values);

    void clear();

    void removeValue(String[] value);

    List<String> getValue();

    default boolean isMasked() {
        return false;
    }

    default String getName() {
        StringBuilder name = new StringBuilder(getLabel().getText());
        return (isMandatory()) ? name.substring(0, name.length()-2) : name.toString();
    }

    default String getError() {
        return getParent().$("*[data-text='error']").getText();
    }

    default boolean isEnabled() {
        return getParent().getAttribute("data-enabled").equalsIgnoreCase("true");
    }

    default boolean isMandatory() {
        return getParent().getAttribute("data-mandatory").equalsIgnoreCase("true");
    }
}
