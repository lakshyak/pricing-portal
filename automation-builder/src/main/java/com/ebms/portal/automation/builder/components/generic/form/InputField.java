package com.ebms.portal.automation.builder.components.generic.form;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import lombok.Getter;
import org.openqa.selenium.Keys;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class InputField implements FormElement {

    @Getter SelenideElement parent;
   // @Getter SelenideElement label;
   @Getter SelenideElement label;
    @Getter
    ElementsCollection label1;
    SelenideElement inputBox;
    @Getter
    ElementsCollection inputBox1;

    public InputField(SelenideElement parent) {
        this.parent = parent;
        this.parent.shouldBe(Condition.visible).scrollIntoView(false);
        inputBox1 = this.parent.$$("input[type='text']");
        //inputBox = this.parent.$("search-bar__input");
        //label = this.parent.$(".custom-form__field__label");
        label1 = this.parent.$$(".custom-form__field__label");
    }

    public void setValue(String... values) {
        clear();
        Arrays.stream(values).forEach(inputBox::sendKeys);
    }

    public void clear() {
        while(inputBox.getValue().length() > 0) inputBox.sendKeys(Keys.BACK_SPACE);
    }

    public void removeValue(String... value) {
        clear();
    }

//    @Override
//    public String getName() {
//        return(label.exists() && label.isDisplayed()) ? FormElement.super.getName() : inputBox.getAttribute("placeholder");
//    }

    public List<String> getValue() {
        return Collections.singletonList(inputBox.getValue());
    }

    @Override
    public boolean isMasked() {
        return inputBox.has(Condition.attribute("type", "password"));
    }
}
