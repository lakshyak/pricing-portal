package com.ebms.portal.automation.builder.components.generic.form;

import com.codeborne.selenide.SelenideElement;

public class RadioField extends CheckboxField implements FormElement {

    public RadioField(SelenideElement parent) {
        super(parent);
    }

    @Override
    public void clear() {
        //do nothing
    }

    @Override
    public void removeValue(String... value) {
        //do nothing
    }
}
