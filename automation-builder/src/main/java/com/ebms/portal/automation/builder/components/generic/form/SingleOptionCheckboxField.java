package com.ebms.portal.automation.builder.components.generic.form;

import com.codeborne.selenide.SelenideElement;

public class SingleOptionCheckboxField extends CheckboxField implements FormElement {

    SelenideElement option;

    public SingleOptionCheckboxField(SelenideElement parent) {
        super(parent);
        option = parent.$("*[data-option]");
    }

    @Override
    public void setValue(String... values) {
        if (option.getAttribute("data-selected").equalsIgnoreCase("false")) option.click();
    }

    @Override
    public void removeValue(String... value) {
        clear();
    }
}
