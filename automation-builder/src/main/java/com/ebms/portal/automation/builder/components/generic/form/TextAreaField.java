package com.ebms.portal.automation.builder.components.generic.form;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import lombok.Getter;
import org.openqa.selenium.Keys;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class TextAreaField implements FormElement {

    @Getter SelenideElement parent;
    @Getter SelenideElement label;
    SelenideElement textarea;

    public TextAreaField(SelenideElement parent) {
        this.parent = parent;
        this.parent.shouldBe(Condition.visible).scrollIntoView(false);
        textarea = this.parent.$("textarea");
        label = this.parent.$("*[data-text='label']");
    }

    public void setValue(String... values) {
        clear();
        Arrays.stream(values).forEach(textarea::sendKeys);
    }

    public void clear() {
        while(textarea.getValue().length() > 0) textarea.sendKeys(Keys.BACK_SPACE);
    }

    public void removeValue(String... value) {
        clear();
    }

    @Override
    public String getName() {
        return(label.exists() && label.isDisplayed()) ? FormElement.super.getName() : textarea.getAttribute("placeholder");
    }

    public List<String> getValue() {
        return Collections.singletonList(textarea.getValue());
    }

    @Override
    public boolean isMasked() {
        return textarea.has(Condition.attribute("type", "password"));
    }
}
