package com.ebms.portal.automation.builder.components.generic.form.date;

import com.ebms.portal.automation.builder.components.generic.form.FormElement;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class DateField implements FormElement {

    @Getter SelenideElement parent;
    @Getter SelenideElement label;
    @Getter SelenideElement calendarButton;
    SelenideElement inputValueDateItem;

    public DateField(SelenideElement parent) {
        this.parent = parent;
        this.parent.shouldBe(Condition.visible).scrollIntoView(false);
        label = parent.$("*[class='custom-form__field__label']");
        inputValueDateItem = parent.$(".custom-form__field__input");
        calendarButton = parent.$(".tabs__action__cal-icon");//.ui-date-range__input__icon
    }

    @Override
    public void setValue(String... values) {
        Arrays.stream(values).forEach(value -> {
            calendarButton.click();
            ReactCalendarComponent calendar = new ReactCalendarComponent();
            calendar.setCalendarDate(value);
            calendar.calendarShouldDisappear();
        });
    }

    @Override
    public void clear() {
        //Do Nothing
    }

    @Override
    public void removeValue(String[] value) {
        //Do Nothing
    }

    @Override
    public List<String> getValue() {
        return Arrays
                .stream(Objects.requireNonNull(inputValueDateItem.getValue()).split("   -   "))
                .collect(Collectors.toList());
    }
}
