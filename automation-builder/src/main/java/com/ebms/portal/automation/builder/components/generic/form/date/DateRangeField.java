package com.ebms.portal.automation.builder.components.generic.form.date;

import com.ebms.portal.automation.builder.components.generic.form.FormElement;
import com.codeborne.selenide.SelenideElement;

import java.util.Arrays;

public class DateRangeField extends DateField implements FormElement {

    public DateRangeField(SelenideElement parent) {
        super(parent);
    }

    @Override
    public void setValue(String... values) {
        Arrays.stream(values).forEach(value -> {
            calendarButton.click();
            ReactCalendarComponent calendar = new ReactCalendarComponent();
            String[] dateFragments = value.split(" - ");
            calendar.setCalendarDateRange(dateFragments[0], dateFragments[1]);
            calendar.calendarShouldDisappear();
        });
    }
}
