package com.ebms.portal.automation.builder.components.generic.form.date;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

import static com.codeborne.selenide.Selenide.$;

@NoArgsConstructor(access = AccessLevel.PACKAGE)
class ReactCalendarComponent {

    private final SelenideElement calendar = $(".react-calendar");
    private final Navigation navigation = new Navigation(calendar.$(".react-calendar__navigation"));
    private final CalendarContainer container = new CalendarContainer(calendar.$(".react-calendar__viewContainer"));

    private void clickLabel(int clicks) {
        for(int i=0; i<clicks; i++) {
            navigation.getLabel().click();
        }
    }

    private void switchToCentury(int targetYear) {
        //Move to right century
        clickLabel(3);
        YearRange century = new YearRange(navigation.getLabel().getText());
        while (!century.yearInRange(targetYear)) {
            if (targetYear < century.getStarting()) {
                navigation.getPrevious().click();
            }
            else {
                navigation.getNext().click();
            }
            century = new YearRange(navigation.getLabel().getText());
        }
    }

    private void switchToDecadeAndSelectYear(int targetYear) {
        switchToCentury(targetYear);
        //Move to right decade
        List<String> decades = container.getCalendarTileLabels();
        decades.forEach(decade -> {
            YearRange decadeRange = new YearRange(decade);
            if (decadeRange.yearInRange(targetYear)) {
                container.selectCalendarTile(decade);
            }
        });
        //Select right year
        container.selectCalendarTile(Integer.toString(targetYear));
    }

    private void setYear(String year) {
        int selectedYear = Integer.parseInt(navigation.getLabel().getText().split(" ")[1]);
        int targetYear = Integer.parseInt(year);
        if (selectedYear != targetYear) {
            switchToDecadeAndSelectYear(targetYear);
        }
    }

    private void setMonth(String month) {
        String currentMonth = LocalDateTime.now().getMonth().toString();
        if (!currentMonth.equalsIgnoreCase(month) && container.getCalendarContainerView() == CalendarContainerView.MONTH) {
            clickLabel(1);
            container.selectCalendarTile(month);
        }
        else if (container.getCalendarContainerView() == CalendarContainerView.YEAR) {
            container.selectCalendarTile(month);
        }
    }

    private void setDay(String day) {
        container.selectCalendarTile(day);
    }

    protected void setCalendarDate(String date) {
        String[] dateFragments = date.split(" ");
        setYear(dateFragments[2]);
        setMonth(dateFragments[1]);
        setDay(dateFragments[0]);
    }
    
    protected void setCalendarDateRange(String startDate, String endDate) {
        setCalendarDate(startDate);
        setCalendarDate(endDate);
    }
    
    public void calendarShouldDisappear() {
        calendar.shouldBe(Condition.hidden);
    }

    static class Navigation {

        @Getter private final SelenideElement next;
        @Getter private final SelenideElement previous;
        @Getter private final SelenideElement label;

        public Navigation(SelenideElement navigation) {
            next = navigation.$(".react-calendar__navigation__next-button");
            previous = navigation.$(".react-calendar__navigation__prev-button");
            label = navigation.$(".react-calendar__navigation__label");
        }
    }

    static class CalendarContainer {

        ElementsCollection calendarTiles;
        SelenideElement containerViewInformation;

        protected CalendarContainer(SelenideElement container) {
            calendarTiles = container
                            .$$(".react-calendar__tile")
                            .exclude(Condition.cssClass("react-calendar__month-view__days__day--neighboringMonth"));
            containerViewInformation = container.$("div");
        }

        protected void selectCalendarTile(String tileLabel) {
            calendarTiles.find(Condition.text(tileLabel)).click();
        }

        protected CalendarContainerView getCalendarContainerView() {
            String attributeClass = containerViewInformation.getAttribute("class");
            if (attributeClass.contains("month")) return CalendarContainerView.MONTH;
            if (attributeClass.contains("year")) return CalendarContainerView.YEAR;
            if (attributeClass.contains("decade")) return CalendarContainerView.DECADE;
            return CalendarContainerView.CENTURY;
        }

        protected List<String> getCalendarTileLabels() {
            return calendarTiles.texts();
        }
    }

    enum CalendarContainerView {
        MONTH,
        YEAR,
        DECADE,
        CENTURY
    }

    static class YearRange {

        @Getter private final int starting;
        @Getter private final int ending;

        protected YearRange(String decade) {
            String[] years = decade.split(" – ");
            starting = Integer.parseInt(years[0]);
            ending = Integer.parseInt(years[1]);
        }

        protected boolean yearInRange(int year) {
            return year >= starting && year <= ending;
        }
    }

}
