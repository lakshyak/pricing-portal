package com.ebms.portal.automation.builder.components.generic.form.dropdowns;

import com.ebms.portal.automation.builder.components.generic.form.FormElement;
import com.codeborne.selenide.SelenideElement;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class MultiSelectDropdownField extends SingleSelectField implements FormElement {

    public MultiSelectDropdownField(SelenideElement parent) {
        super(parent);
    }

    @Override
    public void setValue(String... values) {
        dropdownPlaceholder.click();
        new MultiSelectPopoverComponent().selectOption("All");
        Arrays.stream(values).forEach(v -> new MultiSelectPopoverComponent().selectOption(v.trim()));
        dropdownPlaceholder.click();
    }

    @Override
    public void clear() {
        dropdownPlaceholder.click();
        new MultiSelectPopoverComponent().unselectAllOptions();
        dropdownPlaceholder.click();
    }

    @Override
    public void removeValue(String... values) {
        dropdownPlaceholder.click();
        Arrays.stream(values).forEach(v -> new MultiSelectPopoverComponent().unselectOptions(v));
        dropdownPlaceholder.click();
    }

    @Override
    public List<String> getValue() {
        dropdownPlaceholder.click();
        List<String> values = new MultiSelectPopoverComponent().getSelectedOptions();
        dropdownPlaceholder.click();
        return values;
    }

    @NoArgsConstructor
    private static class MultiSelectPopoverComponent extends PopoverComponent {

        private static final String ATTRIBUTE_OPTION_SELECTED = "data-selected";

        protected void unselectAllOptions() {
            options.forEach((k,v) -> {
                if (v.getAttribute(ATTRIBUTE_OPTION_SELECTED).equalsIgnoreCase("true")) v.click();
            });
        }

        protected void unselectOptions(String option) {
            options.computeIfPresent(option, (key, val) -> {
                if (val.getAttribute(ATTRIBUTE_OPTION_SELECTED).equalsIgnoreCase("true")) val.click();
                return val;
            });
        }

        protected List<String> getSelectedOptions() {
            return options.values().stream()
                    .filter(element -> element.getAttribute(ATTRIBUTE_OPTION_SELECTED).equalsIgnoreCase("true"))
                    .map(SelenideElement::getText)
                    .collect(Collectors.toList());
        }
    }
}
