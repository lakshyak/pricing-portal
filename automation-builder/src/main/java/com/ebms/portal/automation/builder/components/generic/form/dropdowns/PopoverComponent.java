package com.ebms.portal.automation.builder.components.generic.form.dropdowns;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

import static com.codeborne.selenide.Selenide.$;

class PopoverComponent {

    final SelenideElement popover = $("*[data-elem-type='options-list']");
    final Map<String, SelenideElement> options = new HashMap<>();

    protected PopoverComponent() {
        popover.shouldBe(Condition.appear, Duration.ofSeconds(4));
        ElementsCollection collection = popover.$$("*[data-option]");
        collection.forEach(i -> options.put(i.getText().trim(), i));
    }

    protected void selectOption(String option) {
        options.computeIfPresent(option, (key, val) -> {
            options.get(key).scrollIntoView(false);
            options.get(key).shouldBe(Condition.visible);
            options.get(key).click();
            return val;
        });
    }

    protected void popoverShouldDisappear() {
        popover.should(Condition.disappear);
    }
}
