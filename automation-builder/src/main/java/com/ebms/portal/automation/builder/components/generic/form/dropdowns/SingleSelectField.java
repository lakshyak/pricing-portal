package com.ebms.portal.automation.builder.components.generic.form.dropdowns;

import com.ebms.portal.automation.builder.components.generic.form.FormElement;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import lombok.Getter;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.codeborne.selenide.Selenide.$;

public class SingleSelectField implements FormElement {

    @Getter SelenideElement parent;
    @Getter SelenideElement label;
    protected final SelenideElement dropdownPlaceholder;

    public SingleSelectField(SelenideElement parent) {
        this.parent = parent;
        parent.shouldBe(Condition.visible).scrollIntoView(false);
        label = parent.$("*[data-text='label']");
        dropdownPlaceholder = parent.$("div[data-text='placeholder']");
    }

    @Override
    public void setValue(String... values) {
        Arrays.stream(values).forEach(v -> {
            dropdownPlaceholder.click();
            PopoverComponent popoverComponent = new PopoverComponent();
            popoverComponent.selectOption(v.trim());
            popoverComponent.popoverShouldDisappear();
        });
    }

    @Override
    public void clear() {
        //do nothing
    }

    @Override
    public void removeValue(String... values) {
        //do nothing
    }

    @Override
    public List<String> getValue() {
        return Collections.singletonList(dropdownPlaceholder.getText());
    }
}
