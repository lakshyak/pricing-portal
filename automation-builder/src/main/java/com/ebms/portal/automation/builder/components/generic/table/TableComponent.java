package com.ebms.portal.automation.builder.components.generic.table;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.fest.assertions.api.Assertions;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static com.codeborne.selenide.Selenide.$;

public class TableComponent {

    public static final SelenideElement loader = $(".ui-card-preloader__content");//div[data-preloader='table']");
    public static final SelenideElement bodyLoader = $(".ui-table-preloader__body");//.ui-table__body-preloader");
    private final SelenideElement self;
    private final Object definition;

    public TableComponent(SelenideElement element, Object definition) {
        this.definition = definition;
        this.self = element;
        element.should(Condition.appear);
        element.scrollIntoView(false);
        loader.should(Condition.disappear, Duration.ofSeconds(5));
    }

    public void sortColumn(String columnName) {
        SelenideElement tableHead = self.$("thead tr");
        new TableHead(tableHead, definition).sortColumn(columnName);
    }

    private ElementsCollection getTableRows() {
        return self.$$("tbody tr");
    }

    private TableRow getTableRow(String id) {
        bodyLoader.should(Condition.disappear, Duration.ofSeconds(5));
        if (getTableRows().isEmpty()) Assertions.fail("There are no rows in the table.");
        return getTableRows().stream()
                .map(element -> new TableRow(element, definition))
                .filter(row -> row.getIdForRow().equalsIgnoreCase(id))
                .findFirst()
                .orElse(null);
    }

    public Map<String, String> getRowData(String id) {
        TableRow tableRow = getTableRow(id);
        Assertions.assertThat(tableRow).isNotNull().as("Cannot find a row with the defined id: " + id);
        assert tableRow != null;
        return tableRow.getRowCellsWithValues();
    }

    public Map<String, TableRow.DataCell> getDataCell(String id) {
        TableRow tableRow = getTableRow(id);
        Assertions.assertThat(tableRow).isNotNull().as("Cannot find a row with the defined id: " + id);
        assert tableRow != null;
        return tableRow.getRowCells();
    }

    public List<Map<String, String>> getTableData(String... identifiers) {
        List<Map<String, String>> tableData = new ArrayList<>();
        Arrays.stream(identifiers).forEach(id -> tableData.add(getRowData(id)));
        return tableData;
    }
}
