package com.ebms.portal.automation.builder.components.generic.table;

import com.ebms.portal.automation.builder.factory.annotation.ElementMeta;
import com.ebms.portal.automation.builder.factory.annotation.LocateUsing;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.ebms.portal.automation.builder.factory.FactoryUtils;
import com.ebms.portal.automation.builder.factory.annotation.ProcessElementMeta;

import java.lang.reflect.Field;

import static com.ebms.portal.automation.builder.factory.annotation.ProcessElementMeta.getSelenideElement;

public class TableHead {

    private final SelenideElement head;
    private final Object columns;
    protected TableHead(SelenideElement head, Object definition) {
        this.head = head;
        this.columns = definition;

    }

    private Column getColumnHead(String columnName) {
        Field field = FactoryUtils.getFieldFromVisibleName(columnName, columns);
        return new Column(head, field);
    }

    public void sortColumn(String columnName) {
        getColumnHead(columnName).sortColumn();
    }

    private static class Column {
        private SelenideElement sort;

        public Column(SelenideElement heading, Field field) {
            String locator = field.getAnnotation(ElementMeta.class).locator();
            LocateUsing strategy = field.getAnnotation(ElementMeta.class).locateUsing();
            SelenideElement cell = ProcessElementMeta.getSelenideElement(strategy, locator, heading);
            if (cell.$("div[data-action-name='sort']").exists()) sort = cell.$("div[data-action-name='sort']");
        }

        private void waitForLoadComplete() {
            TableComponent.loader.should(Condition.appear);
            TableComponent.loader.should(Condition.disappear);
        }

        public void sortColumn() {
            sort.click();
            waitForLoadComplete();
        }

    }
}
