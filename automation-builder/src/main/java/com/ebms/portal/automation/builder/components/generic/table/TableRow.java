package com.ebms.portal.automation.builder.components.generic.table;

import com.ebms.portal.automation.builder.factory.annotation.ActionOnly;
import com.ebms.portal.automation.builder.factory.annotation.ElementMeta;
import com.ebms.portal.automation.builder.factory.annotation.LocateUsing;
import com.codeborne.selenide.SelenideElement;
import com.ebms.portal.automation.builder.factory.FactoryUtils;
import com.ebms.portal.automation.builder.factory.annotation.ProcessElementMeta;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

import static com.ebms.portal.automation.builder.factory.annotation.ProcessElementMeta.getSelenideElement;

public class TableRow {

    private final SelenideElement self;
    private final Object definition;
    private final String id;

    protected TableRow(SelenideElement row, Object definition) {
        self = row;
        this.definition = definition;
        this.id = self.getAttribute("data-row");
    }

    public DataCell getTableCell(String cellName) { ;
        Field field = FactoryUtils.getFieldFromVisibleName(cellName, definition);
        return new DataCell(self, field);
    }

    public SelenideElement getElement() {
        return self;
    }

    public String getIdForRow() {
        return id;
    }

    public String getValueForCell(String cellName) {
        return getTableCell(cellName).getCellText();
    }

    public Map<String, String> getRowCellsWithValues() {
        return Arrays.stream(definition.getClass().getDeclaredFields())
                .filter(field -> !field.isAnnotationPresent(ActionOnly.class))
                .map(field -> new DataCell(self, field))
                .collect(Collectors.toMap(DataCell::getName, DataCell::getCellText));
    }

    public Map<String, DataCell> getRowCells() {
        return Arrays.stream(definition.getClass().getDeclaredFields())
                .filter(field -> !field.isAnnotationPresent(ActionOnly.class))
                .map(field -> new DataCell(self, field))
                .collect(Collectors.toMap(DataCell::getName, DataCell::getSelf));
    }

    public static class DataCell {

        private final String name;
        private final SelenideElement cell;

        public DataCell(SelenideElement row, Field field) {
            name = field.getAnnotation(ElementMeta.class).elementName();
            String locator = field.getAnnotation(ElementMeta.class).locator();
            LocateUsing strategy = field.getAnnotation(ElementMeta.class).locateUsing();
            cell = ProcessElementMeta.getSelenideElement(strategy, locator, row);
        }

        public String getName() {
            return name;
        }

        public String getCellText() {
            return cell.getText().replace("\n", " ");
        }

        public DataCell getSelf() {
            return this;
        }

        public SelenideElement getCell() {
            return cell;
        }
    }
}
