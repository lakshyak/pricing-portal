package com.ebms.portal.automation.builder.factory;

import com.ebms.portal.automation.builder.components.generic.form.FormElement;
import com.ebms.portal.automation.builder.factory.annotation.ElementMeta;
import com.ebms.portal.automation.builder.factory.annotation.ProcessElementMeta;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static org.fest.assertions.api.Assertions.fail;

public class FactoryMethods {

    private final Object object;
    private final SelenideElement parent;

    public FactoryMethods(Object object, SelenideElement parent) {
        this.object = object;
        this.parent = parent;
    }

    private Field getFieldFromVisibleName(String visibleFieldName) {
        return FactoryUtils.getFieldsForPlatform(object).stream()
                .filter(field -> field.getAnnotation(ElementMeta.class).elementName()
                        .equalsIgnoreCase(visibleFieldName))
                .findFirst()
                .orElse(null);
    }

    private boolean isSelenideElement(Field field) {
        return field.getType().getName().contains("SelenideElement");
    }

    private void failForFieldNotFound(String visibleName) {
        fail("Unable to find the field referenced with name " + visibleName);
    }

    public void setValue(String visibleName, String ...values) throws IllegalAccessException {
        Field field = getFieldFromVisibleName(visibleName);
        if (field != null) {
            if (isSelenideElement(field))
                Arrays.stream(values).forEach(Objects.requireNonNull(ProcessElementMeta.getSelenideElement(field, parent))::setValue);
            else {
                boolean accessible = field.canAccess(object);
                field.setAccessible(true);
                FormElement component = (FormElement)field.get(object);
                component.setValue(values);
                field.setAccessible(accessible);
            }
        }
        else failForFieldNotFound(visibleName);
    }

    public void clear(String visibleName) throws IllegalAccessException {
        Field field = getFieldFromVisibleName(visibleName);
        if (field != null) {
            if (isSelenideElement(field)) Objects.requireNonNull(ProcessElementMeta.getSelenideElement(field, parent)).clear();
            else {
                boolean accessible = field.canAccess(object);
                field.setAccessible(true);
                FormElement component = (FormElement)field.get(object);
                component.clear();
                field.setAccessible(accessible);
            }
        }
        else failForFieldNotFound(visibleName);
    }

    public void removeValue(String visibleName, String ...values) throws IllegalAccessException {
        Field field = getFieldFromVisibleName(visibleName);
        if (field != null) {
            if (isSelenideElement(field)) Objects.requireNonNull(ProcessElementMeta.getSelenideElement(field, parent)).clear();
            else {
                boolean accessible = field.canAccess(object);
                field.setAccessible(true);
                FormElement component = (FormElement)field.get(object);
                component.removeValue(values);
                field.setAccessible(accessible);
            }
        }
        else {
            failForFieldNotFound(visibleName);
        }
    }

    public String getName(String visibleName) throws IllegalAccessException {
        Field field = getFieldFromVisibleName(visibleName);
        if (field != null) {
            if (field.getType().getName().contains("SelenideElement"))
                return Objects.requireNonNull(ProcessElementMeta.getSelenideElement(field, parent)).getText();
            else {
                boolean accessible = field.canAccess(object);
                field.setAccessible(true);
                FormElement component = (FormElement)field.get(object);
                field.setAccessible(accessible);
                return component.getName();
            }
        }
        else {
            fail("Unable to find the field referenced with name " + visibleName);
        }
        return null;
    }

    public String getError(String visibleName) throws IllegalAccessException {
        Field field = getFieldFromVisibleName(visibleName);
        if (field != null) {
            if (isSelenideElement(field)) return Objects.requireNonNull(ProcessElementMeta.getSelenideElement(field, parent)).getText();
            else {
                boolean accessible = field.canAccess(object);
                field.setAccessible(true);
                FormElement component = (FormElement)field.get(object);
                field.setAccessible(accessible);
                return component.getError();
            }
        }
        else failForFieldNotFound(visibleName);
        return null;
    }

    public boolean isFieldEnabled(String visibleName) throws IllegalAccessException {
        Field field = getFieldFromVisibleName(visibleName);
        if (field != null) {
            if (isSelenideElement(field)) return !Objects.requireNonNull(ProcessElementMeta.getSelenideElement(field, parent))
                                                    .has(Condition.attribute("disabled"));
            else {
                boolean accessible = field.canAccess(object);
                field.setAccessible(true);
                FormElement component = (FormElement)field.get(object);
                field.setAccessible(accessible);
                return component.isEnabled();
            }
        }
        else failForFieldNotFound(visibleName);
        return false;
    }

    public boolean isMasked(String visibleName) throws IllegalAccessException {
        Field field = getFieldFromVisibleName(visibleName);
        if (field != null) {
            if (isSelenideElement(field)) {
                return Objects.requireNonNull(ProcessElementMeta.getSelenideElement(field, parent))
                        .has(Condition.attribute("type", "password"));
            }
            else {
                boolean accessible = field.canAccess(object);
                field.setAccessible(true);
                FormElement component = (FormElement)field.get(object);
                field.setAccessible(accessible);
                return component.isMasked();
            }
        }
        else failForFieldNotFound(visibleName);
        return false;
    }

    public List<String> getValue(String visibleName) throws IllegalAccessException {
        Field field = getFieldFromVisibleName(visibleName);
        if (field != null) {
            if (isSelenideElement(field)) {
                return Collections.singletonList(Objects.requireNonNull(ProcessElementMeta.getSelenideElement(field, parent)).getText());
            }
            else {
                boolean accessible = field.canAccess(object);
                field.setAccessible(true);
                FormElement component = (FormElement)field.get(object);
                field.setAccessible(accessible);
                return component.getValue();
            }
        }
        else failForFieldNotFound(visibleName);
        return Collections.emptyList();
    }
}
