package com.ebms.portal.automation.builder.factory;

import com.ebms.portal.automation.builder.factory.annotation.ElementMeta;
import com.ebms.portal.automation.builder.factory.annotation.WebSpecificField;
import com.ebms.portal.automation.builder.factory.annotation.MobileSpecificField;
import com.ebms.portal.automation.builder.factory.annotation.ProcessElementMeta;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.fest.assertions.api.Assertions.fail;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class FactoryUtils {

    public static Field getFieldFromVisibleName(String visibleFieldName, Object object) {
        return FactoryUtils.getFieldsForPlatform(object).stream()
                .filter(field -> field.getAnnotation(ElementMeta.class).elementName()
                        .equalsIgnoreCase(visibleFieldName))
                .findFirst()
                .orElse(null);
    }

    public static Object getQualifiedElement(Field field, SelenideElement parent, String requester) {
        SelenideElement element = ProcessElementMeta.getSelenideElement(field, parent);
        String name = field.getName();
        String type = field.getType().getTypeName();
        if (!field.getType().getName().contains("SelenideElement")) {
            try {
                return field.getType().getDeclaredConstructor(SelenideElement.class).newInstance(element);
            }
            catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                String error = "Class <" + requester + "> was unable to declare the field <" + name + "> " +
                        "of <" +  type + "> type.";
                e.printStackTrace();
                fail(error);
            }
        }
        return element;
    }

    private static Object convertSelenideElementToObject(Class<?> objectClass, SelenideElement element, String field, String requester) {
        try {
            return objectClass.getDeclaredConstructor(SelenideElement.class).newInstance(element);
        }
        catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            String error = "Class <" + requester + "> was unable to declare the field <" + field + "> " +
                           "as a list of <" +  objectClass.getName() + "> type.";
            fail(error);
        }
        return null;
    }

    public static List<Object> getQualifiedElements(Field field, SelenideElement parent, String requester) {
        ElementsCollection elements = ProcessElementMeta.getElementCollection(field, parent);
        String fieldName = field.getName();
        assert elements != null;
        if (elements.isEmpty()) return Collections.singletonList(elements);
        if (!field.getType().getName().contains("ElementsCollection")) {
            List<Object> objectList = new ArrayList<>();
            ParameterizedType parameterizedType = (ParameterizedType) field.getGenericType();
            Class<?> objectClass = (Class<?>) parameterizedType.getActualTypeArguments()[0];
            for (SelenideElement element : elements) {
                if (objectClass.getName().contains("SelenideElement")) objectList.add(element);
                else objectList.add(convertSelenideElementToObject(objectClass, element, fieldName, requester));
            }
            return objectList;
        }
        return Collections.singletonList(elements);
    }

    private static boolean isPlatformMobile() {
        RemoteWebDriver remoteWebDriver = ((RemoteWebDriver) WebDriverRunner.getWebDriver());
        return remoteWebDriver.getCapabilities().getPlatform().is(Platform.ANDROID) ||
                remoteWebDriver.getCapabilities().getPlatform().is(Platform.IOS);
    }

    private static boolean isMobileBrowser() {
        RemoteWebDriver remoteWebDriver = ((RemoteWebDriver) WebDriverRunner.getWebDriver());
        String userAgent = (String) remoteWebDriver.executeScript("return navigator.userAgent;");
        return userAgent.contains("Android") || userAgent.contains("Mobile Safari");
    }

    public static boolean isViewMobile() {
        return isPlatformMobile() || isMobileBrowser();
    }

    public static List<Field> getFieldsForPlatform(Object object) {
        if (isViewMobile()) {
            return Arrays.stream(object.getClass().getDeclaredFields())
                    .filter(field -> field.isAnnotationPresent(ElementMeta.class) &&
                            !field.isAnnotationPresent(WebSpecificField.class))
                    .collect(Collectors.toList());
        }
        else {
            return Arrays.stream(object.getClass().getDeclaredFields())
                    .filter(field -> field.isAnnotationPresent(ElementMeta.class) &&
                            !field.isAnnotationPresent(MobileSpecificField.class))
                    .collect(Collectors.toList());
        }
    }
}
