package com.ebms.portal.automation.builder.factory;

import com.ebms.portal.automation.builder.components.BasePageComponent;
import com.codeborne.selenide.SelenideElement;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.lang.reflect.Field;
import java.util.List;

import static com.codeborne.selenide.Selenide.$;
import static org.fest.assertions.api.Assertions.fail;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ObjectFactory {

    private static void initializeField(Field field, Object requester, SelenideElement parent) {
        try {
            boolean accessible = field.canAccess(requester);
            field.setAccessible(true);
            Object element = FactoryUtils.getQualifiedElement(field, parent, requester.getClass().getName());
            field.set(requester, element);
            field.setAccessible(accessible);
        }
        catch (IllegalAccessException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }

    private static void initializeObjectCollection(Field field, Object requester, SelenideElement parent) {
        try {
            boolean accessible = field.canAccess(requester);
            field.setAccessible(true);
            String className = field.getType().getName();
            List<Object> elements = FactoryUtils.getQualifiedElements(field, parent, requester.getClass().getName());
            if (elements.isEmpty()) fail("No elements found for the field :" + field.getName());
            if (className.contains("ElementsCollection")) field.set(requester, elements.get(0));
            else field.set(requester, elements);
            field.setAccessible(accessible);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }

    private static void initializeFields(Object requester, SelenideElement parent) {
        List<Field> fields = FactoryUtils.getFieldsForPlatform(requester);
        for (Field field: fields) {
            String[] packageAndClass = field.getType().getName().split("\\.");
            String fieldType = packageAndClass[packageAndClass.length - 1];
            if (fieldType.equalsIgnoreCase("ElementsCollection") || fieldType.equalsIgnoreCase("List")) initializeObjectCollection(field, requester, parent);
            else initializeField(field, requester, parent);
        }
    }

    private static void updateRequester(BasePageComponent<?> requester, SelenideElement parent) {
        FactoryMethods methods = new FactoryMethods(requester, parent);
        requester.setMethods(methods);
        requester.setObject(requester);
        requester.setTitle($("*[data-text]='title'"));
    }

    public static void init(BasePageComponent<?> requester) {
        updateRequester(requester, null);
        initializeFields(requester, null);
    }

    public static void init(BasePageComponent<?> requester, SelenideElement parent) {
        updateRequester(requester, parent);
        requester.setInitializedWithParent(true);
        initializeFields(requester, parent);
    }
}
