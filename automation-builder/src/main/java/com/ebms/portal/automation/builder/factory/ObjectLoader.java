package com.ebms.portal.automation.builder.factory;

import com.ebms.portal.automation.builder.components.BasePageComponent;
import com.ebms.portal.automation.builder.factory.annotation.ObjectLoaderMeta;
import com.ebms.portal.automation.builder.factory.annotation.Platform;
import com.ebms.portal.automation.builder.factory.annotation.ProcessElementMeta;
import com.ebms.portal.automation.builder.factory.annotation.WaitForLoad;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.fest.assertions.api.Fail.fail;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ObjectLoader {

    private static AnnotationConfigApplicationContext context;
    private static String componentPackage;

    /**
     * This function returns the first occurrence of method @ObjectLoadMeta depending on the platform where the portal was opened.
     * @param methods These are the declared methods in a given base page component.
     * @param className This is the name of the class passed to the function to throw <br> relevant error message.
     * @return It returns the Method annotated with @ObjectLoadMeta.
     * @author Jatin
     */
    private static Method getObjectLoaderMethod(Method[] methods, String className) {
        boolean isMobile = FactoryUtils.isViewMobile();
        Method method = (isMobile) ?
                Arrays.stream(methods)
                        .filter(m -> m.isAnnotationPresent(ObjectLoaderMeta.class) &&
                                !m.getAnnotation(ObjectLoaderMeta.class).platform().equals(Platform.WEB))
                        .findFirst()
                        .orElse(null)
                :
                Arrays.stream(methods)
                        .filter(m -> m.isAnnotationPresent(ObjectLoaderMeta.class) &&
                                !m.getAnnotation(ObjectLoaderMeta.class).platform().equals(Platform.MOBILE))
                        .findFirst()
                        .orElse(null);
        String platform = (isMobile) ? "MOBILE" : "WEB";
        if (method == null) fail("Object Loader not found for the " + platform +  " platform in the class" + className);
        return method;
    }

    /**
     * A page component is said to be loaded in browser in case all its fields are loaded successfully.
     * This function validates the same and is used
     * @param fieldList It is the list of the fields in the Page Component
     * @author Jatin
     */
    private static void applySelenideWaitOnFields(List<Field> fieldList) {
        for (Field field : fieldList) {
            SelenideElement e = ProcessElementMeta.getSelenideElement(field, null);
            assert e != null;
            e.should(Condition.appear, Duration.ofSeconds(10));
        }
    }

    /**
     * This function is used by the ObjectLoader to wait for the page component to load.
     * @param objectClass This is the class for the page component on which wait needs to be applied.
     * @throws NoSuchMethodException - Throws when class is missing a No Arguments Constructor.
     * @throws IllegalAccessException - Throws when No Arguments Constructor is not set to public.
     * @author Jatin
     */
    private static void waitForObjectToLoad(Class<?> objectClass) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Object object = objectClass.getDeclaredConstructor().newInstance();
        List<Field> allFields = FactoryUtils.getFieldsForPlatform(object);
        List<Field> waitForLoadFields = allFields.stream()
                .filter(f -> f.isAnnotationPresent(WaitForLoad.class)).collect(Collectors.toList());
        if (!waitForLoadFields.isEmpty()) applySelenideWaitOnFields(waitForLoadFields);
        else applySelenideWaitOnFields(allFields);
    }

    /**
     * This is used to set the parent package which contains the Page Components.
     * @param componentPackage A string variable pointing to the package name for the Page Components.
     * @author Jatin
     */
    public static void setComponentPackage(String componentPackage) {
        ObjectLoader.componentPackage = componentPackage;
    }

    /**
     * This is a validation method that utilizes waitForObjectToLoad to check if the page component has loaded. It throws an error (not exception) in the page object does not load.
     * @param componentName A string variable that points to the Page Component
     * @author Jatin
     */
    public static void hasComponentLoaded(String componentName) {
        Class<?> objectClass = context.getBean(componentName).getClass();
        try {
            waitForObjectToLoad(objectClass);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method:
     * <ul>
     *     <li>Loads the page component in browser based on name provided by the user.</li>
     *     <li>Waits for the page component to completely load.</li>
     *     <li>Enriches the page component (when loaded without parent context).</li>
     * </ul>
     * @param componentName - Name of the page component to be loaded.
     * @return A generic instance <T> of type BaseComponent.
     */
    @SuppressWarnings("unchecked")
    public static  <T extends BasePageComponent<T>> T getComponent(String componentName) {
        try {
            context = new AnnotationConfigApplicationContext(componentPackage);
            Class<?> objectClass = context.getBean(componentName).getClass();
            Method method = getObjectLoaderMethod(objectClass.getDeclaredMethods(), objectClass.getName());
            if (method != null) {
                Object object = method.invoke(objectClass.getDeclaredConstructor().newInstance());
                BasePageComponent<T> basePageComponent = (BasePageComponent<T>) object;
                waitForObjectToLoad(objectClass);
                if (!basePageComponent.isInitializedWithParent()) ObjectFactory.init(basePageComponent);
                return (T) basePageComponent;
            }
        }
        catch (InvocationTargetException e) {
            e.getTargetException().printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
