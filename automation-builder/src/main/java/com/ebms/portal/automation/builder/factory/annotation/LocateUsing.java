package com.ebms.portal.automation.builder.factory.annotation;

public enum LocateUsing {
    ID,
    CSS,
    XPATH,
    TEXT,
    PARTIAL_TEXT,
    TITLE,
    VALUE,
    CLASS,
    LINK_TEXT,
    PARTIAL_LINK_TEXT,
    ATTRIBUTE_AND_VALUE,
    NAME
}
