package com.ebms.portal.automation.builder.factory.annotation;

public enum Platform {
    WEB,
    MOBILE,
    COMMON
}
