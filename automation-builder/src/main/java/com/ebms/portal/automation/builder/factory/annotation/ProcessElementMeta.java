package com.ebms.portal.automation.builder.factory.annotation;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.openqa.selenium.NoSuchElementException;

import java.lang.reflect.Field;
import java.util.EnumMap;
import java.util.Map;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static org.fest.assertions.api.Assertions.fail;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ProcessElementMeta {

    public static SelenideElement getSelenideElement(LocateUsing locatedBy, String value, SelenideElement parent) {
        switch (locatedBy) {
            case ID:
                return (parent == null) ? $(byId(value)) : parent.$(byId(value));
            case CSS:
                return (parent == null) ? $(value) : parent.$(value);
            case XPATH:
                return (parent == null) ? $(byXpath(value)) : parent.$(byXpath(value));
            case TEXT:
                return (parent == null) ? $(byText(value)) : parent.$(byText(value));
            case PARTIAL_TEXT:
                return (parent == null) ? $(withText(value)) : parent.$(withText(value));
            case TITLE:
                return (parent == null) ? $(byTitle(value)) : parent.$(byTitle(value));
            case VALUE:
                return (parent == null) ? $(byValue(value)) : parent.$(byValue(value));
            case CLASS:
                return (parent == null) ? $(byClassName(value)) : parent.$(byClassName(value));
            case LINK_TEXT:
                return (parent == null) ? $(byLinkText(value)) : parent.$(byLinkText(value));
            case PARTIAL_LINK_TEXT:
                return (parent == null) ? $(byPartialLinkText(value)) : parent.$(byPartialLinkText(value));
            case ATTRIBUTE_AND_VALUE:
                String[] s = value.split("::");
                return (parent == null) ? $(byAttribute(s[0], s[1])) : parent.$(byAttribute(s[0], s[1]));
            default: case NAME:
                return (parent == null) ? $(byName(value)) : parent.$(byName(value));
        }
    }

    public static ElementsCollection getElementCollection(LocateUsing locatedBy, String value, SelenideElement parent) {
        switch (locatedBy) {
            case ID:
                return (parent == null) ? $$(byId(value)) : parent.$$(byId(value));
            case CSS:
                return (parent == null) ? $$(value) : parent.$$(value);
            case XPATH:
                return (parent == null) ? $$(byXpath(value)) : parent.$$(byXpath(value));
            case TEXT:
                return (parent == null) ? $$(byText(value)) : parent.$$(byText(value));
            case PARTIAL_TEXT:
                return (parent == null) ? $$(withText(value)) : parent.$$(withText(value));
            case TITLE:
                return (parent == null) ? $$(byTitle(value)) : parent.$$(byTitle(value));
            case VALUE:
                return (parent == null) ? $$(byValue(value)) : parent.$$(byValue(value));
            case CLASS:
                return (parent == null) ? $$(byClassName(value)) : parent.$$(byClassName(value));
            case LINK_TEXT:
                return (parent == null) ? $$(byLinkText(value)) : parent.$$(byLinkText(value));
            case PARTIAL_LINK_TEXT:
                return (parent == null) ? $$(byPartialLinkText(value)) : parent.$$(byPartialLinkText(value));
            case ATTRIBUTE_AND_VALUE:
                String[] s = value.split("::");
                return (parent == null) ? $$(byAttribute(s[0], s[1])) : parent.$$(byAttribute(s[0], s[1]));
            default: case NAME:
                return (parent == null) ? $$(byName(value)) : parent.$$(byName(value));
        }
    }

    public static Map<LocatorProperties, Object> getDefinedProperties(Field f) {
        if (!f.isAnnotationPresent(ElementMeta.class))
            fail("Unable to process the field " +  f.getName() + " because it is not decorated with @ElementsMeta annotation.");
        EnumMap<LocatorProperties, Object> properties = new EnumMap<>(LocatorProperties.class);
        properties.put(LocatorProperties.LOCATE_USING, f.getAnnotation(ElementMeta.class).locateUsing());
        properties.put(LocatorProperties.LOCATOR, f.getAnnotation(ElementMeta.class).locator());
        properties.put(LocatorProperties.ELEMENT_NAME, f.getAnnotation(ElementMeta.class).elementName());
        return properties;
    }

    public static SelenideElement getSelenideElement(Field f, SelenideElement parent) {
        Map<LocatorProperties, Object> properties = getDefinedProperties(f);
        LocateUsing locateUsing = (LocateUsing) properties.get(LocatorProperties.LOCATE_USING);
        String locator = (String) properties.get(LocatorProperties.LOCATOR);
        String elementName = (String) properties.get(LocatorProperties.ELEMENT_NAME);
        try {
            return getSelenideElement(locateUsing, locator, parent);
        }
        catch (NoSuchElementException e) {
            fail("Unable to locate Selenide Element for field " + f.getName() + " decorated with name " + elementName + ".");
        }
        return null;
    }

    public static ElementsCollection  getElementCollection(Field f, SelenideElement parent) {
        Map<LocatorProperties, Object> properties = getDefinedProperties(f);
        LocateUsing locateUsing = (LocateUsing) properties.get(LocatorProperties.LOCATE_USING);
        String locator = (String) properties.get(LocatorProperties.LOCATOR);
        String elementName = (String) properties.get(LocatorProperties.ELEMENT_NAME);
        try {
            return getElementCollection(locateUsing, locator, parent);
        }
        catch (NoSuchElementException e) {
            fail("Unable to locate Element Collection for field " + f.getName() + " decorated with name " + elementName + ".");
        }
        return null;
    }
}

enum LocatorProperties {
    LOCATE_USING,
    LOCATOR,
    ELEMENT_NAME
}
