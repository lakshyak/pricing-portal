/**
 * Custom common annotations to turn page components declarative in nature.
 */
package com.ebms.portal.automation.builder.factory.annotation;
