package com.ebms.portal.automation.builder.utils;

import lombok.Getter;

import java.util.Map;
import java.util.Objects;

@Getter
public class PlatformConfiguration {

    String platform;
    String os;
    String browser;
    String userAgent;
    String device;
    String app;
    int screenWidth;
    int screenHeight;

    public PlatformConfiguration(Map<String, String> yamlConfiguration) {
        platform = yamlConfiguration.computeIfAbsent("platform", k -> null);
        os = yamlConfiguration.computeIfAbsent("os", k -> null);
        browser = yamlConfiguration.computeIfAbsent("browser", k -> null);
        userAgent = yamlConfiguration.computeIfAbsent("userAgent", k -> null);
        device = yamlConfiguration.computeIfAbsent("device", k -> null);
        app = yamlConfiguration.computeIfAbsent("app", k -> null);
        screenWidth = Integer.parseInt(Objects.requireNonNull(yamlConfiguration.computeIfAbsent("screenWidth", k -> "1024")));
        screenHeight = Integer.parseInt(Objects.requireNonNull(yamlConfiguration.computeIfAbsent("screenHeight", k -> "768")));
    }
}
