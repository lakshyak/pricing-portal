package com.ebms.portal.automation.pages;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.SelenideElement;
import com.ebms.portal.automation.builder.components.BasePageComponent;
import com.ebms.portal.automation.builder.components.generic.NavBar;
import com.ebms.portal.automation.builder.factory.annotation.*;
import com.ebms.portal.automation.pages.dashboard.components.FinancialWidget;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Getter
@NoArgsConstructor
@Component("Dashboard Page")
public class DashboardPage extends BasePageComponent<DashboardPage> {

    @WaitForLoad
    @WebSpecificField
    @ElementMeta(elementName = "Financial Widget", locator = "div[class='reports-summary  reports-summary--financial']")
    FinancialWidget financialWidget;
    //reports-summary  reports-summary--financial

    @WaitForLoad
    @WebSpecificField
    @ElementMeta(elementName = "Primary Navigation", locator = "ul[class='header__nav--major']")
    NavBar primaryNavigation;

    @WaitForLoad
    @WebSpecificField
    @ElementMeta(elementName = "Hierarchy Filter", locator = ".multi-select-dropdown__trigger__content__selection")
    SelenideElement hierarchyFilter;

    @WaitForLoad
    @WebSpecificField
    @ElementMeta(elementName = "First Hierarchy Filter", locator = ".multi-select-dropdown__trigger__content__selection__selected-options")
    SelenideElement firstHierarchyFilter;

    @WaitForLoad
    @WebSpecificField
    @ElementMeta(elementName = "Second Hierarchy Filter", locator = ".multi-select-dropdown__trigger__content__selection__more-options")
    SelenideElement secondHierarchyFilter;


    @ObjectLoaderMeta(platform = Platform.WEB)
    public DashboardPage loadWeb() throws InterruptedException {
        return this;
    }
}