package com.ebms.portal.automation.pages;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.SelenideElement;
import com.ebms.portal.automation.builder.components.BasePageComponent;
import com.ebms.portal.automation.builder.factory.ObjectFactory;
import com.ebms.portal.automation.builder.factory.annotation.ElementMeta;
import com.ebms.portal.automation.builder.factory.annotation.ObjectLoaderMeta;
import com.ebms.portal.automation.builder.factory.annotation.Platform;
import com.ebms.portal.automation.builder.factory.annotation.WebSpecificField;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.openqa.selenium.By;
import org.springframework.stereotype.Component;

import static com.codeborne.selenide.Selenide.$;

@Getter
@NoArgsConstructor
@Component("Group Administration Page")
public class GroupAdministrationPage extends BasePageComponent<GroupAdministrationPage> {

    @WebSpecificField
    @ElementMeta(elementName = "Hierarchy Link", locator = "span[class*='filter-wrapper--text']")
    SelenideElement hierarchyLink;


    public void selectLocationInHierarchyLink(String location) {
        $(By.xpath("//span[text()='The Townsend Corporation - 00614']/following-sibling::span")).click();
        $(By.xpath("//span[text()='"+location+"']")).click();
    }

    @ObjectLoaderMeta(platform = Platform.WEB)
    public GroupAdministrationPage loadWeb() {
        return this;
    }
}
