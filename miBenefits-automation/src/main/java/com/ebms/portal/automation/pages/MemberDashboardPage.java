package com.ebms.portal.automation.pages;

import com.codeborne.selenide.SelenideElement;
import com.ebms.portal.automation.builder.components.generic.NavBar;
import com.ebms.portal.automation.builder.factory.annotation.ElementMeta;
import com.ebms.portal.automation.builder.factory.annotation.WaitForLoad;
import com.ebms.portal.automation.builder.factory.annotation.WebSpecificField;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Getter
@NoArgsConstructor
@Component("MemberDashboardPage")
public class MemberDashboardPage {

    @WaitForLoad
    @WebSpecificField
    @ElementMeta(elementName = "miBenefits", locator = ".header__logo__image")
    SelenideElement miBenfits;

    @WaitForLoad
    @WebSpecificField
    @ElementMeta(elementName = "BenefitsandCoverage", locator = ".header__nav__link[href='/member/benefits']")
    SelenideElement BenefitsandCoverage;

    @WaitForLoad
    @WebSpecificField
    @ElementMeta(elementName = "Claims", locator = ".header__nav__link[href='/member/claims']")
    SelenideElement Claims;

    @WaitForLoad
    @WebSpecificField
    @ElementMeta(elementName = "ClaimsandServices", locator = ".header__nav__label")
    SelenideElement ClaimsandServices;

    @WaitForLoad
    @WebSpecificField
    @ElementMeta(elementName = "Language", locator = "li[class='header__action--minor'] div span[class='header__nav__icon__details']")
    SelenideElement Language;

    @WaitForLoad
    @WebSpecificField
    @ElementMeta(elementName = "DocumentLibrary", locator = "body > div:nth-child(4) > div:nth-child(6) > div:nth-child(1) > div:nth-child(1) > header:nth-child(1) > ul:nth-child(3) > li:nth-child(2)")
    SelenideElement DocumentLibrary;

    @WaitForLoad
    @WebSpecificField
    @ElementMeta(elementName = "Contact", locator = "body > div:nth-child(4) > div:nth-child(6) > div:nth-child(1) > div:nth-child(1) > header:nth-child(1) > ul:nth-child(3) > li:nth-child(3)")
    SelenideElement Contact;

    @WaitForLoad
    @WebSpecificField
    @ElementMeta(elementName = "Notifications", locator = "body > div:nth-child(4) > div:nth-child(6) > div:nth-child(1) > div:nth-child(1) > header:nth-child(1) > ul:nth-child(3) > li:nth-child(4)")
    SelenideElement Notifications;

    @WaitForLoad
    @WebSpecificField
    @ElementMeta(elementName = "Account", locator = "body > div:nth-child(4) > div:nth-child(6) > div:nth-child(1) > div:nth-child(1) > header:nth-child(1) > ul:nth-child(3) > li:nth-child(5)")
    SelenideElement ClaiAccountms;

    @WaitForLoad
    @WebSpecificField
    @ElementMeta(elementName = "Logout", locator = "body > div:nth-child(4) > div:nth-child(6) > div:nth-child(1) > div:nth-child(1) > header:nth-child(1) > ul:nth-child(3) > li:nth-child(6)")
    SelenideElement Logout;


}
