package com.ebms.portal.automation.pages;

import com.codeborne.selenide.*;
import com.ebms.portal.automation.builder.components.BasePageComponent;
import com.ebms.portal.automation.builder.factory.annotation.*;
import com.ebms.portal.automation.builder.factory.ObjectFactory;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.fest.assertions.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.springframework.stereotype.Component;

@Getter
@NoArgsConstructor
@Component("Membership Page")
public class MembershipPage extends BasePageComponent<MembershipPage> {


    public MembershipPage(SelenideElement memobj) {
        ObjectFactory.init(this, memobj);
    }

    int count = 0;

    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "Registration Statics", locator = "div[class='claims-overview__item census__chart']")
    SelenideElement census_chart;


    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "Registration Statistics", locator = "span[class='census__registered-stats__attributes__item__value']")
    ElementsCollection statistics;


    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "Total Statistics", locator = "tspan[class='claims-overview__graph__value']")
    SelenideElement totalstatisticswithcomma;


    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "Statistics Label", locator = "span[class='census__registered-stats__attributes__item__label']")
    ElementsCollection statisticslabel;

    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "Invite New Employee", locator = "div[class='census__head__actions']")
    SelenideElement btn_invitenewemployee;

    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "Search Box", locator = "#searchMember")
    SelenideElement search_member;

    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "Download Button", locator = "div[class='table__column--cvs']")
    SelenideElement download_btn;


    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "Table Heading", locator = ".table__head")
    ElementsCollection tableHead;


    @WebSpecificField
    @ElementMeta(elementName = "Table Heading-REGISTRATION STATUS", locator = "span[id='registrationstatus']")
    SelenideElement registration;


    @WebSpecificField
    @ElementMeta(elementName = "Table Heading-RELATIONSHIP", locator = "span[id='relationship']")
    SelenideElement relate;

    @WebSpecificField
    @ElementMeta(elementName = "Filter pop up Relationship", locator = "li[data-id='relationship']")
    ElementsCollection relationshipfilter;

    @WebSpecificField
    @ElementMeta(elementName = "Filter pop up Registration", locator = "li[data-id='registrationstatus']")
    ElementsCollection registrationtypefilter;


    @WebSpecificField
    @ElementMeta(elementName = "Membership Listing Table", locator = "tr[class='table__row']")
    ElementsCollection rowcount_membershiptable;


    public void fetchtotalofstatistics() {

        for (int i = 0; i <= 3; i++) {

            String s = statistics.get(i).getText();
            s = s.replace(",", "");
            count = count + Integer.parseInt(s);
        }

        String totalstatistics = totalstatisticswithcomma.getText();
        totalstatistics = totalstatistics.replace(",", "");

        Assertions.assertThat(count).isEqualTo(Integer.parseInt(totalstatistics));
        //if(count == Integer.parseInt(totalstatistics))
        System.out.println("Total of Statistics is equal to the sum of Registered Members + Registered Dependents" +
                "+ Non-Registered Members+ Non-Registered Dependents:" + count + ":" + totalstatistics);
    }

    public void memberlistingdata_registypefilter(String regitype) {
        registration.scrollIntoView("{behavior: \"instant\", block: \"center\", inline: \"center\"}").click();
        registrationtypefilter.get(2).click();
        System.out.println("Filtered on the basis of Registration Status: " + regitype);
    }

    public void memberlistingdata_relationshipfilter(String relation) {
        relate.scrollIntoView("{behavior: \"instant\", block: \"center\", inline: \"center\"}").click();
        relationshipfilter.findBy(Condition.text(relation)).click();
        System.out.println("Filtered on the basis of Relationship: " + relation);
    }


    public void rowcount() {

//        JavascriptExecutor js = (JavascriptExecutor) WebDriverRunner.getWebDriver();
//        js.executeScript("window.scrollTo(0, document.documentElement.scrollHeight);");
        WebDriverRunner.getWebDriver().findElement(By.tagName("body")).sendKeys(Keys.END);
        String s;
      //  int size = rowcount_membershiptable.size();
        int size =rowcount_membershiptable.size();
        var reachedend = false;
        int newcount = size;
        if (statisticslabel.get(0).getText().equals("Registered Members")) {
            s = statistics.get(0).getText();
            s = s.replace(",", "");
            int statcount = Integer.parseInt(s);

            while (!reachedend) {
                WebDriverRunner.getWebDriver().findElement(By.tagName("body")).sendKeys(Keys.END);
            //    js.executeScript("window.scrollTo(0, document.documentElement.scrollHeight);");
                newcount = newcount + rowcount_membershiptable.size();
                if (newcount == size)
                    reachedend = true;
                else
                    size = newcount;
            }
            Assertions.assertThat(statcount).isEqualTo(size);

        }

    }


    @ObjectLoaderMeta(platform = Platform.WEB)
    public MembershipPage loadWeb() {
        Configuration.holdBrowserOpen = true;
        return this;
    }

}
