package com.ebms.portal.automation.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.ebms.portal.automation.builder.components.BasePageComponent;
import com.ebms.portal.automation.builder.factory.annotation.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.openqa.selenium.By;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static com.codeborne.selenide.Selenide.$;

@Getter
@NoArgsConstructor
@Component("Reports Page")
public class ReportsPage extends BasePageComponent<ReportsPage> {

    Map<String, SelenideElement> reportFiltersMap = new HashMap<>();

    public void ReportsPage() {

    }
    @WaitForLoad
    @WebSpecificField
    @ElementMeta(elementName = "Hierarchy Link", locator = "span[class*='filter-wrapper--text']")
    SelenideElement hierarchyLink;

    @WaitForLoad
    @WebSpecificField
    @ElementMeta(elementName = "Report Filters", locator = "span[class*='select-box__heading']")
    ElementsCollection reportFilters;

    public void setReportFilters(ElementsCollection elementsCollection) {
        for (SelenideElement element : elementsCollection) {
            reportFiltersMap.put(element.getText(), element);
        }
    }

    public void selectGroupInHierarchyLink(String group) {
        $(By.xpath("//li[@class='filter-wrapper--list__item']//span[text()='" + group + "']")).click();
    }

    public void selectValueInReportFilter(String filter, String value) {
        setReportFilters(reportFilters);
        SelenideElement s = reportFiltersMap.get(filter);
        s.click();
        $(By.xpath("//span[text()='" + value + "']")).click();
    }


    @ObjectLoaderMeta(platform = Platform.WEB)
    public ReportsPage loadWeb() {
        return this;
    }
}
