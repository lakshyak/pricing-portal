package com.ebms.portal.automation.pages.dashboard.components;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.ebms.portal.automation.builder.components.BasePageComponent;
import com.ebms.portal.automation.builder.factory.annotation.ElementMeta;
import com.ebms.portal.automation.builder.factory.annotation.WaitForLoad;
import com.ebms.portal.automation.builder.factory.annotation.WebSpecificField;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Getter
@NoArgsConstructor
@Component("Financial Widget")
public class FinancialWidget extends BasePageComponent<FinancialWidget> {

    @Getter
    Map<String, SelenideElement> tabs = new HashMap<>();

    @Getter
    Map<String, String> chargetype = new HashMap<>();
    SelenideElement parentTab;
    SelenideElement parentChargeType;

    public FinancialWidget(SelenideElement parentWidget) {
        this.parentTab = parentWidget;
        ElementsCollection widgetTabs = parentTab.$$("li");
        for(SelenideElement tab:widgetTabs){
            if(!tab.getText().equals(""))
         tabs.put(tab.getText(),tab);
            else
                tabs.put("Calendar",tab);
        }

        ElementsCollection widgetchargetypes = parentTab.$$(".tile__section--data");
        for(SelenideElement charge:widgetchargetypes){
            SelenideElement dataLabel = charge.$(".tile__section--data__label");
            SelenideElement dataValue = charge.$(".tile__section--data__value ");
            chargetype.put(dataLabel.getText(),dataValue.getText());
        }

    }

}
