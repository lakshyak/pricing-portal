package com.ebms.portal.automation.pages.dashboard.components;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.ebms.portal.automation.builder.components.BasePageComponent;
import com.ebms.portal.automation.builder.factory.ObjectFactory;
import com.ebms.portal.automation.builder.factory.annotation.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.openqa.selenium.By;
import org.springframework.stereotype.Component;

import static com.codeborne.selenide.Selenide.$;

@Getter
@NoArgsConstructor
@Component("Hierarchy Filter")
public class HierarchyFilter extends BasePageComponent<HierarchyFilter> {

//    public HierarchyFilter(SelenideElement hierarchyFilter) {
//        ObjectFactory.init(this, hierarchyFilter);
//    }

    @WaitForLoad
    @WebSpecificField
    @ElementMeta(elementName = "Hierarchy Filter", locator = ".multi-select-dropdown__trigger__content__selection")
    SelenideElement hierarchyFilter;

    @WebSpecificField
    @ElementMeta(elementName = "Select All Checkbox", locator = "div[class='hierarchy-wrapper--header__select-all'] div[class='kit-checkbox kit-checkbox--checked']")
    SelenideElement selectAllCheckbox;

    @WebSpecificField
    @ElementMeta(elementName = "Group Textbox", locator = "input[placeholder$='Search Group']")
    SelenideElement groupTextbox;

    @WebSpecificField
    @ElementMeta(elementName = "Location Textbox", locator = "input[placeholder$='Search Location']")
    SelenideElement locationTextbox;

    @WebSpecificField
    @ElementMeta(elementName = "Apply button", locator = "button[id='apply'] div[class='kit-button__content']")
    SelenideElement applyButton;


    @WebSpecificField
    @ElementMeta(elementName = "First Hierarchy Filter", locator = ".multi-select-dropdown__trigger__content__selection__selected-options")
    SelenideElement firstHierarchyFilter;


    @WebSpecificField
    @ElementMeta(elementName = "Second Hierarchy Filter", locator = ".multi-select-dropdown__trigger__content__selection__more-options")
    SelenideElement secondHierarchyFilter;


    public void selectLocationDepartment(String dep, String location) {
        selectAllCheckbox.click();
        locationTextbox.setValue(location);
        $(By.xpath("//span[text()='Townsend Arborcare']")).shouldNotBe(Condition.visible);
        checkCheckboxByLabel(dep);
        applyButton.click();
    }

    public void selectLocation(String location) {
        locationTextbox.clear();
        locationTextbox.setValue(location);
        checkCheckboxByLabel(location);
    }

    public void selectGroup(String group) {
        groupTextbox.clear();
        groupTextbox.setValue(group);
        checkCheckboxByLabel(group);
    }

    public void checkCheckboxByLabel(String label) {
        $(By.xpath("//*[text()='" + label + "']/parent::div//div[contains(@class,'kit-checkbox')]")).click();
    }

    public void uncheckCheckboxByLabel(String label) {
        $(By.xpath("//*[text()='" + label + "']/parent::div//div[contains(@class,'kit-checkbox kit-checkbox--checked')]")).click();
    }


    @ObjectLoaderMeta(platform = Platform.WEB)
    public HierarchyFilter loadWeb() {
        return this;
    }
}
