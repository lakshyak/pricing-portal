package com.ebms.portal.automation.pages.login;

import com.codeborne.selenide.SelenideElement;
import com.ebms.portal.automation.builder.components.BasePageComponent;
import com.ebms.portal.automation.builder.factory.annotation.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Getter
@NoArgsConstructor
@Component("Forgot Password Request Code Page")
public class ForgotPasswordRequestCodePage extends BasePageComponent<ForgotPasswordRequestCodePage> {

    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "Username Email", locator = "label[for='1']")
    SelenideElement codeOption1;

    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "", locator = "label[for='2']")
    SelenideElement codeOption2;

    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "Request A Verification Code", locator = "button[type='submit']")
    SelenideElement requestVerificationCode;

    @ObjectLoaderMeta(platform = Platform.WEB)
    public ForgotPasswordRequestCodePage loadWeb() {
//        Configuration.holdBrowserOpen = true;
        return this;
    }
}
