package com.ebms.portal.automation.pages.login;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.SelenideElement;
import com.ebms.portal.automation.builder.components.BasePageComponent;
import com.ebms.portal.automation.builder.factory.annotation.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Getter
@NoArgsConstructor
@Component("Forgot Password Username Email Page")
public class ForgotPasswordUsernameEmailPage extends BasePageComponent<ForgotPasswordUsernameEmailPage> {

    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "Username Email", locator = "input[type='text']")
    SelenideElement usernameEmail;

    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "Next", locator = ".card__footer.card__footer--modal.email-assist__confirmation")
    SelenideElement next;

    @WebSpecificField
    @ElementMeta(elementName = "Error Message", locator = ".error-message--server")
    SelenideElement errorMessage;

    @ObjectLoaderMeta(platform = Platform.WEB)
    public ForgotPasswordUsernameEmailPage loadWeb() {
//        Configuration.holdBrowserOpen = true;
        return this;
    }
}
