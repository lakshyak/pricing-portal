package com.ebms.portal.automation.pages.login;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.SelenideElement;
import com.ebms.portal.automation.builder.components.BasePageComponent;
import com.ebms.portal.automation.builder.factory.ObjectFactory;
import com.ebms.portal.automation.builder.factory.annotation.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Getter
@NoArgsConstructor
@Component("Forgot Username Member Message Page")
public class ForgotUsernameMemberMessagePage extends BasePageComponent<ForgotUsernameMemberMessagePage> {

    public ForgotUsernameMemberMessagePage(SelenideElement forgotUsrnmePwdObj) {
        ObjectFactory.init(this, forgotUsrnmePwdObj);
    }

    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "Member Message", locator = ".card--order-id-card__body__success")
    SelenideElement memberMessage;

    @ObjectLoaderMeta(platform = Platform.WEB)
    public ForgotUsernameMemberMessagePage loadWeb() {
//        Configuration.holdBrowserOpen = true;
        return this;
    }
}
