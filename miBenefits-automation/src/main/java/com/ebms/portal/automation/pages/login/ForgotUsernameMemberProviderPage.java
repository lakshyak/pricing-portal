package com.ebms.portal.automation.pages.login;

import com.codeborne.selenide.SelenideElement;
import com.ebms.portal.automation.builder.components.BasePageComponent;
import com.ebms.portal.automation.builder.factory.annotation.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.openqa.selenium.WebElement;
import org.springframework.stereotype.Component;
import java.util.List;

@Getter
@NoArgsConstructor
@Component("Forgot Username Member Provider Page")
public class ForgotUsernameMemberProviderPage extends BasePageComponent<ForgotUsernameMemberProviderPage> {

    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "Member/ProviderSelect", locator = ".select-box__heading")
    SelenideElement memberProviderSelect;

    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "Member Select", locator = "li[class='select-box-list__item select-box-list__item__selected'] span[class='select-box-list__item__text']")
    SelenideElement memberSelect;

    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "Provider Select", locator = "li[class='select-box-list__item'] span[class='select-box-list__item__text']")
    SelenideElement providerSelect;

    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "MemberID", locator = "#memberId-validator")
    SelenideElement memberID;


    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "Next", locator = "button[type='submit']")
    SelenideElement next;

    @WebSpecificField
    @ElementMeta(elementName = "Error Message", locator = ".error-message--server")
    SelenideElement errorMessage;

//
//    @WebSpecificField
//    @WaitForLoad
//    @ElementMeta(elementName = "ProviderSelect", locator = ".select-box__heading")
//    SelenideElement providerSelect;

    @ObjectLoaderMeta(platform = Platform.WEB)
    public ForgotUsernameMemberProviderPage loadWeb() {
        //   Configuration.holdBrowserOpen = true;
        return this;
    }

}
