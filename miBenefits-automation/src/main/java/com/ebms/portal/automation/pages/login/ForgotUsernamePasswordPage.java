package com.ebms.portal.automation.pages.login;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.SelenideElement;
import com.ebms.portal.automation.builder.components.BasePageComponent;
import com.ebms.portal.automation.builder.factory.ObjectFactory;
import com.ebms.portal.automation.builder.factory.annotation.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Getter
@NoArgsConstructor
@Component("Forgot Username Password Page")
public class ForgotUsernamePasswordPage extends BasePageComponent<ForgotUsernamePasswordPage> {

    public ForgotUsernamePasswordPage(SelenideElement forgotUsrnmePwdObj) {
        ObjectFactory.init(this, forgotUsrnmePwdObj);
    }

    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "Back to Login", locator = ".login__back-button")
    SelenideElement backToLogin;

    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "Forgot Username", locator = "div[class='card--login pre-animation'] li:nth-child(1)")
    SelenideElement iForgotMyUsername;

    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "Forgot Password", locator = "div[class='card--login pre-animation'] li:nth-child(2)")
    SelenideElement iForgotMyPassword;

    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "Register Now", locator = ".card--login__footer__link.register-tooltip")
    SelenideElement registerNow;

    @ObjectLoaderMeta(platform = Platform.WEB)
    public ForgotUsernamePasswordPage loadWeb() {
       // Configuration.holdBrowserOpen = true;
        return this;
    }
}


