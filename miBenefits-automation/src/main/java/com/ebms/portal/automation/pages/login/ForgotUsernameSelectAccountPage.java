package com.ebms.portal.automation.pages.login;

import com.codeborne.selenide.SelenideElement;
import com.ebms.portal.automation.builder.components.BasePageComponent;
import com.ebms.portal.automation.builder.factory.annotation.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Getter
@NoArgsConstructor
@Component("Forgot Username Select Account Page")
public class ForgotUsernameSelectAccountPage extends BasePageComponent<ForgotUsernameSelectAccountPage> {

    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "Select Account Message", locator = ".card__header__title.card__header__title--already-registered")
    SelenideElement selectAccountMessage;

    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "Account 1 Elements", locator = "body > div:nth-child(3) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > li:nth-child(1) > div:nth-child(1) > div:nth-child(3) > div:nth-child(1)")
    SelenideElement Account1Elements;

    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "Account 2 Elements", locator = "body > div:nth-child(3) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > li:nth-child(1) > div:nth-child(1) > div:nth-child(3) > div:nth-child(2) > div:nth-child(1)")
    SelenideElement Account2Elements;

    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "Continue", locator = ".registration-cards__action[data-id='10931']")
    SelenideElement accountSelect;

    @ObjectLoaderMeta(platform = Platform.WEB)
    public ForgotUsernameSelectAccountPage loadWeb() {
        // Configuration.holdBrowserOpen = true;
        return this;
    }
}
