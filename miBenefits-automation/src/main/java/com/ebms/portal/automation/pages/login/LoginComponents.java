package com.ebms.portal.automation.pages.login;

import com.ebms.portal.automation.builder.components.BasePageComponent;
import com.ebms.portal.automation.builder.factory.ObjectFactory;
import com.ebms.portal.automation.builder.factory.annotation.ElementMeta;
import com.ebms.portal.automation.builder.factory.annotation.WaitForLoad;
import com.ebms.portal.automation.builder.factory.annotation.WebSpecificField;
import com.codeborne.selenide.SelenideElement;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Getter
@NoArgsConstructor
@Component("Member Login Components")
public class LoginComponents extends BasePageComponent<LoginComponents> {

    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "error Message", locator = "div[data-text='error']")
    SelenideElement errorMessage;

    public LoginComponents(SelenideElement memberLoginComponents) {
        ObjectFactory.init(this, memberLoginComponents);
    }
}
