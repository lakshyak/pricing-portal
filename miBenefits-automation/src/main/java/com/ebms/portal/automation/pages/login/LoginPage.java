package com.ebms.portal.automation.pages.login;

import com.codeborne.selenide.Configuration;
import com.ebms.portal.automation.builder.components.BasePageComponent;
import com.ebms.portal.automation.builder.factory.annotation.*;
import com.codeborne.selenide.Credentials;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.ebms.portal.automation.builder.factory.ObjectFactory;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.fest.assertions.api.Assertions;
import org.springframework.stereotype.Component;
import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

@Getter
@NoArgsConstructor
@Component("Login Page")
public class LoginPage extends BasePageComponent<LoginPage> {

    public LoginPage(SelenideElement loginobj) {
        ObjectFactory.init(this, loginobj);
  }

    // Login Specific Elements
    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "Username", locator = "#username")
    SelenideElement username;

    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "Password", locator = "#password")
    SelenideElement password;

    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "Sign Button", locator = ".login__button--sign-in")
    SelenideElement loginButton;

    // Need Help Components
    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "needHelp", locator = ".card--login__header__link")
    SelenideElement needHelp;

    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "registerNow", locator = ".card--login__footer__link.register-tooltip")
    SelenideElement RegisterNow;



//    @WebSpecificField
//    @WaitForLoad
//    @ElementMeta(elementName = "Email Error Message", locator = "div[data-input-field='username']")
//    MemberLoginComponents emailErrorMessage;
//
//    @WebSpecificField
//    @WaitForLoad
//    @ElementMeta(elementName = "Password Error Message", locator = "div[data-password-field='password']")
//    MemberLoginComponents passwordErrorMessage;

    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "Login Form", locator = ".card--login__page")
    public LoginValidation loginForm;

    public List<Map<String, String>> getYamlConfiguration(String fileName) {
        Yaml yaml = new Yaml();
        InputStream inputStream = LoginPage.class
                .getClassLoader()
                .getResourceAsStream(fileName);
        return yaml.load(inputStream);
    }
    public Map<String,String> getUser(String name) {
        List<Map<String,String>> users = getYamlConfiguration("userUatGPA.yaml");
        for(Map<String,String> user : users) {
            if (user.get("user").equals(name)) return user;
        }
        Assertions.fail("Requested user doesn't exist");
        return null;
    }

    public String getURL(String server) {
        List<Map<String,String>> URLs = getYamlConfiguration("URL.yaml");
        for(Map<String,String> URL : URLs) {
            if (URL.get("environment").equals(server)) return URL.get("URL");
        }
        Assertions.fail("Requested URL doesn't exist");
        return null;
    }

    public void login(String userDetails) {
        Credentials credentials = new Credentials(getUser(userDetails).get("username"), getUser(userDetails).get("password"));
        username.sendKeys(credentials.login);
        password.sendKeys(credentials.password);
        loginButton.click();
    }

//    @ElementMeta(elementName = "Login Links", locator = "div[class$='ui-member-login-page__card']")
//    NavBar loginLinks;

    @ObjectLoaderMeta(platform = Platform.WEB)
    public LoginPage loadWeb() {
        Selenide.open(getURL("gpauat"));
//        Configuration.holdBrowserOpen = true;
        return this;
    }
}