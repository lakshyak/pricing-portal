package com.ebms.portal.automation.pages.login;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

public class LoginValidation {
    @Getter
    SelenideElement parent;

    public LoginValidation(SelenideElement parent) {
        this.parent = parent;
    }

    public List<String> getValidationErrorMessages() {
        ElementsCollection validationErrorMessagesFields = parent.$$(".error-message--form-field");
        List<String> validationErrorMessages = new ArrayList<>();
        for (SelenideElement validationErrorMessagesField : validationErrorMessagesFields) {
            validationErrorMessages.add(validationErrorMessagesField.getText());
        }
        return validationErrorMessages;
    }

    public String getValidationErrorMessage(String validationType) {
        SelenideElement validationErrorMessagesField;
        if (validationType.equals("username")) {
            validationErrorMessagesField = parent.$("#username-error");
        } else if (validationType.equals("password")) {
            validationErrorMessagesField = parent.$("#password-error");
        } else if (validationType.equals("serverError")) {
            validationErrorMessagesField = parent.$(".error-message--server").shouldBe(Condition.visible);
        } else throw new IllegalArgumentException();
        return validationErrorMessagesField.getText();

    }
}
