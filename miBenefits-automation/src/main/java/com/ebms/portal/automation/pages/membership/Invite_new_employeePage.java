package com.ebms.portal.automation.pages.membership;


import com.codeborne.selenide.*;
import com.ebms.portal.automation.builder.components.BasePageComponent;
import com.ebms.portal.automation.builder.components.generic.form.InputField;
import com.ebms.portal.automation.builder.factory.annotation.*;
import com.ebms.portal.automation.builder.factory.ObjectFactory;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

import static com.codeborne.selenide.Selenide.$$;

@Getter
@Setter
@NoArgsConstructor
@Component("Invite New Employee Page")
public class Invite_new_employeePage extends BasePageComponent<Invite_new_employeePage>{


    public Invite_new_employeePage(SelenideElement invitenewemp) {ObjectFactory.init(this, invitenewemp);}

    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "Title", locator = "h1[class='census__invite-member__title']")
    SelenideElement formtitle;


    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "Parent", locator = "form[class='enrollment__form enrollment__form__section__wrapper']")
    InputField parent;


    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "First Name", locator = "#firstName")
    SelenideElement firstname;

    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "Last Name", locator = "#lastName")
    SelenideElement lastname;


    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "Date of Birth", locator = "#dateOfBirth")
    SelenideElement dob;

    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "Date of Hire", locator = "#dateOfHire")
    SelenideElement doh;

    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "SSN", locator = "#ssn")
    SelenideElement ssn;

    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "Email ID", locator = "#emailId")
    SelenideElement email;

    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "Location", locator = "span[class='reset-selected-text']")
    SelenideElement location;

    @WebSpecificField
    @ElementMeta(elementName = "Location", locator = "li[class='filter-wrapper--list__item']")
    ElementsCollection listoflocation;

    //div[@class='select-box__wrapper select-box__wrapper--open']

    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "Employee ID", locator = "#employeeId")
    SelenideElement empid;

    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "Annual Salary", locator = "#salary")
    SelenideElement salary;


    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "Invite", locator = "button[type='submit']")
    SelenideElement invitebutton;

    public void fetchinputlist(String fname, String lname, String DOB,
                               String DOH, String SSN, String Emailid, String loc, String Employeeid, String annualsalary){

        ElementsCollection list = parent.getInputBox1();

        for(int i =0; i<=7; i++){

            switch (i){

                case 0:
                    list.get(0).setValue(fname);
                    break;
                case 1:
                    list.get(1).setValue(lname);
                    break;
                case 2:
                    list.get(2).setValue(DOB);
                    break;
                case 3:
                    list.get(3).setValue(DOH);
                    break;
                case 4:
                    list.get(4).setValue(SSN);
                    break;
                case 5:
                    list.get(5).setValue(Emailid);
                    break;
                case 6:
                    list.get(6).setValue(Employeeid);
                    break;
                case 7:
                    list.get(7).setValue(annualsalary);
                    break;

            }

        }
        location.click();
        listoflocation.findBy(Condition.text(loc)).click();
        invitebutton.click();

    }

    @ObjectLoaderMeta(platform = Platform.WEB)
    public Invite_new_employeePage loadWeb() {
        //   Configuration.holdBrowserOpen = true;
        return this;

    }

}
