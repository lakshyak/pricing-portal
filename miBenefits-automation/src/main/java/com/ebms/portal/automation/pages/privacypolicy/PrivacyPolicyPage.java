package com.ebms.portal.automation.pages.privacypolicy;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.SelenideElement;
import com.ebms.portal.automation.builder.components.BasePageComponent;
import com.ebms.portal.automation.builder.factory.annotation.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Getter
@NoArgsConstructor
@Component("Privacy Policy Page")
public class PrivacyPolicyPage extends BasePageComponent<PrivacyPolicyPage> {

    @WebSpecificField
    @WaitForLoad
    @ElementMeta(elementName = "Privacy Policy", locator = ".footer--mi-benefits__link[ket='privacyPolicy']")
    SelenideElement privacyPolicy;

    @ObjectLoaderMeta(platform = Platform.WEB)
    public PrivacyPolicyPage loadWeb() {
        Configuration.holdBrowserOpen = false;
        return this;
    }
}