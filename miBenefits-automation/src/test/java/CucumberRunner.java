
import com.ebms.portal.automation.builder.utils.PlatformConfiguration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.List;
import java.util.Map;

@CucumberOptions(
        strict = true,
        plugin = {"com.ebms.reportportal.custom.reporter.ScenarioReporter"},
        tags = "@scenario"


)
public class CucumberRunner extends AbstractTestNGCucumberTests {
    String userAgent;
    int screenWidth;
    int screenHeight;

    public static List<Map<String, String>> getYamlConfiguration() {
        Yaml yaml = new Yaml();
        InputStream inputStream = CucumberRunner.class
                                    .getClassLoader()
                                    .getResourceAsStream("browser-config.yaml");
        return yaml.load(inputStream);

    }

    public static PlatformConfiguration[][] getPlatformConfigurations(List<Map<String, String>> platforms) {
        PlatformConfiguration[][] configurations = new PlatformConfiguration[platforms.size()][1];
        for(int i=0; i<platforms.size(); i++) {
            configurations[i][0] = new PlatformConfiguration(platforms.get(i));
        }
        return configurations;
    }

    @DataProvider(name = "getPlatforms")
    public static Object[][] readDeviceConfigurations() {
        return getPlatformConfigurations(getYamlConfiguration());
    }

    @Factory(dataProvider = "getPlatforms")
    public CucumberRunner(PlatformConfiguration configuration) {
        userAgent = configuration.getUserAgent();
        screenWidth = configuration.getScreenWidth();
        screenHeight = configuration.getScreenHeight();
    }

    @BeforeMethod
    public void beforeMethod() throws MalformedURLException {
//        URL zaleniumHub = new URL("http://ac2842071cfe940b39e831946b662968-1077934501.us-west-1.elb.amazonaws.com:4444/wd/hub/");
//        ChromeOptions options = new ChromeOptions();
//        options.setHeadless(false);
//        if (userAgent != null) options.addArguments("--user-agent=", userAgent);
//        WebDriver driver = new RemoteWebDriver(zaleniumHub, options);
//        WebDriverRunner.setWebDriver(driver);
//        if (userAgent != null) {
//            WebDriverRunner.getWebDriver().manage().window().setSize(new Dimension(screenWidth, screenHeight));
//        }
//        else {
//            driver.manage().window().maximize();
//        }
        Selenide.open();
        WebDriverRunner.getWebDriver().manage().window().maximize();
    }

    @AfterMethod
    public void afterMethod() {
         Selenide.closeWebDriver();
    }

    @Override
    @DataProvider(parallel = true)
    public Object[][] scenarios() {
       //return super.scenariosCucumberRunner.runScenario();
        return super.scenarios();
    }
}
