package com.ebms.portal.automation.JDBC;

import lombok.Getter;
import org.fest.assertions.api.Assertions;
import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;
import java.sql.*;
import java.util.*;

@Getter
public class DataBaseConnection {

    @Getter
    Connection connection;

    public List<Map<String, String>> getYamlConfiguration() {
        Yaml yaml = new Yaml();
        InputStream inputStream = DataBaseConnection.class
                .getClassLoader()
                .getResourceAsStream("DBConnection.yaml");
        return yaml.load(inputStream);
    }

    public DataBaseConnection(String databaseName) {
        try {
            Map<String, String> dbDetails = readDbDetails(databaseName);
            Class.forName(dbDetails.get("driver"));
            connection = DriverManager.getConnection(dbDetails.get("host"), dbDetails.get("username"), dbDetails.get("password"));
            System.out.println("Connected to DB");
        }
        catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    public Map<String,String> readDbDetails(String name){
        List<Map<String,String>> credsDetails = getYamlConfiguration();
        for(Map<String,String> dbDetails : credsDetails) {
            if (dbDetails.get("DbName").equals(name)) return dbDetails;
        }
        Assertions.fail("Requested DB details doesn't exist");
        return null;
    }

    public ArrayList<String> getResultSet(String query, String columnName) {
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);
            ResultSetMetaData rsd = rs.getMetaData();
            System.out.println("Total Column Count is  >>>>>>>>>" + rsd.getColumnCount());
            System.out.println("Results" + rs);
            ArrayList<String> items = new ArrayList<>();
            while (rs.next()) {
                items.add(rs.getString(columnName));
            }
            return items;
        }
        catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
        return null;
    }

    public ArrayList<String> getResultSets(String query, String... columnName) {
        ArrayList<String> items = new ArrayList<>();
        try {
            for (int i = 0; i < Arrays.stream(columnName).count(); i++) {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(query);
                while ((rs.next())) {
                    System.out.println(items.add(rs.getString((columnName[i]))));
                }
//            return items;
            }
        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
        return items;
    }


    public HashMap<String, String> getResultSetsWithHeaders(String query, String... columnName) {
        HashMap<String, String> items = new LinkedHashMap<>();
        try{
            for (int i = 0; i <Arrays.stream(columnName).count(); i++) {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(query);
                while (rs.next()) {
                    System.out.println(items.put(columnName[i], rs.getString(columnName[i])));
                }
            }
        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
        return items;
    }


    public void closeConnection() {
        try {
            connection.close();
        }
        catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }
}
