package com.ebms.portal.automation.context;

import com.ebms.portal.automation.builder.components.BasePageComponent;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Getter
@Setter
public class Context {
    public Logger logger = LoggerFactory.getLogger(Context.class);
    BasePageComponent<?> component;
}
