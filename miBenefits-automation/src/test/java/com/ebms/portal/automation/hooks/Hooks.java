package com.ebms.portal.automation.hooks;

import com.ebms.portal.automation.context.Context;
import com.ebms.portal.automation.builder.factory.ObjectLoader;
import com.codeborne.selenide.WebDriverRunner;
import io.cucumber.core.api.Scenario;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

public class Hooks {

    Context context;

    public Hooks(Context context) {
        this.context = context;
    }

    @Before
    public void beforeHook() {
        ObjectLoader.setComponentPackage("com.ebms.portal.automation.pages");
    }

    @After
    public void afterHook(Scenario scenario) {
        if (scenario.isFailed()) {
            context.logger.info(
                    "RP_MESSAGE#BASE64#{}#{}",
                    ((TakesScreenshot) WebDriverRunner.getWebDriver()).getScreenshotAs(OutputType.BASE64),
                    "Logging Base64 Image."
            );
        }
    }
}
