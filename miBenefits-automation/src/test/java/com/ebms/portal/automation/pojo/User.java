package com.ebms.portal.automation.pojo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class User {
    public String name;
    public String username;
    public String password;
}
