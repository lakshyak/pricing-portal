package com.ebms.portal.automation.steps;

import com.ebms.portal.automation.context.Context;
import com.ebms.portal.automation.builder.factory.ObjectLoader;
import com.ebms.portal.automation.pages.DashboardPage;
import com.ebms.portal.automation.pages.login.LoginPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class StepsCommon {

    Context context;

    public StepsCommon(Context context) {
        this.context = context;
    }
//
//    @Given("The user enters the login details and clicks on the login button")
//    public void theUserEntersTheLoginDetailsAndClicksOnTheLoginButton() {
//        LoginPage login = ObjectLoader.getComponent("Login Page");
//
//        login.login("Member");
//
//    }

    @Given("The user enters the login details and clicks on the login button for {string}")
    public void theUserEntersTheLoginDetailsAndClicksOnTheLoginButtonFor(String user) {
        LoginPage login = ObjectLoader.getComponent("Login Page");
        login.login(user);
    }

    @Given("^The user should navigate to the (.*)$")
    public void userNavigatesToComponent(String componentName) {
        context.setComponent(ObjectLoader.getComponent(componentName));
    }


    @Given("^User should navigate to page (.*)$")
    public void userNavigatesToPage(String page) {
        DashboardPage dashboardPage = ObjectLoader.getComponent("Dashboard Page");
        assert dashboardPage != null;
        dashboardPage.getPrimaryNavigation().navigateTo(page);
    }

    @And("The user should click on {string} link and navigate to {string}")
    public void theUserShouldClickOnEnrollmentsLinkAndNavigateTo(String link, String page) {
        DashboardPage dashboardPage = ObjectLoader.getComponent("Dashboard Page");
        $(By.xpath("//span[text()='"+link+"']")).click();
        assert dashboardPage != null;
        dashboardPage.getPrimaryNavigation().navigateTo(page);
    }
}
