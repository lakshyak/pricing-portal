package com.ebms.portal.automation.steps;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.conditions.Visible;
import com.ebms.portal.automation.builder.factory.ObjectLoader;
import com.ebms.portal.automation.context.Context;
import com.ebms.portal.automation.pages.DashboardPage;
import com.ebms.portal.automation.pages.dashboard.components.HierarchyFilter;
import com.ebms.portal.automation.pages.dashboard.components.HierarchyFilter;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.fest.assertions.internal.Conditions;
import org.testng.Assert;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StepsDashboard {

    Context context;

    public StepsDashboard(Context context) {
        this.context = context;
    }


    @Then("Below links should be displayed in navigation bar")
    public void belowLinksShouldBeDisplayedInNavigationBar(DataTable table) {
        DashboardPage dashboardPage = ObjectLoader.getComponent("Dashboard Page");
        Map<String, String> expectedLinksMap = table.asMap(String.class, String.class);
        assert dashboardPage != null;
        Map<String, SelenideElement> actualLinksMap = dashboardPage.getPrimaryNavigation().getLinks();
        Set<String> expectedLinks = expectedLinksMap.keySet();
        Set<String> actualLinks = actualLinksMap.keySet();
        Assert.assertEquals(expectedLinks, actualLinks);
        System.out.println("Test");
    }

    @When("^The user clicks on Membership button on dashboard$")
    public void theuserclicksonmembershipbuttonondashboard() {
        DashboardPage maindashboardpage = ObjectLoader.getComponent("Dashboard Page");
        assert maindashboardpage != null;
        maindashboardpage.getPrimaryNavigation().navigateTo("Membership");
    }


    @Then("Below tabs should be displayed in Financial widget")
    public void belowTabsShouldBeDisplayedInFinancialWidget(DataTable table) {
        DashboardPage dashboardPage = ObjectLoader.getComponent("Dashboard Page");
        assert dashboardPage != null;
        Map<String, SelenideElement> actualTabsMap = dashboardPage.getFinancialWidget().getTabs();
        List<String> actualTabsList = new ArrayList<String>(actualTabsMap.keySet()).stream().sorted().collect(Collectors.toList());
        List<String> expectedTabsList = table.asList().stream().sorted().collect(Collectors.toList());
        Assert.assertEquals(actualTabsList, expectedTabsList);
    }

    @And("Below charge types should be displayed in Financial Widget")
    public void belowChargeTypesShouldBeDisplayedInFinancialWidget(DataTable table) {
        DashboardPage dashboardPage = ObjectLoader.getComponent("Dashboard Page");
        assert dashboardPage != null;
        Map<String, String> actualChargeMap = dashboardPage.getFinancialWidget().getChargetype();
        List<String> actualChargesList = new ArrayList<String>(actualChargeMap.keySet()).stream().sorted().collect(Collectors.toList());
        List<String> expectedChargeList = table.asList().stream().sorted().collect(Collectors.toList());
        Assert.assertEquals(actualChargesList, expectedChargeList);
    }
}
