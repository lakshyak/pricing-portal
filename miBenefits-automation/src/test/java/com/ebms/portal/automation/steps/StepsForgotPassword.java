package com.ebms.portal.automation.steps;

import com.codeborne.selenide.Condition;
import com.ebms.portal.automation.builder.factory.ObjectLoader;
import com.ebms.portal.automation.context.Context;
import com.ebms.portal.automation.pages.login.ForgotPasswordRequestCodePage;
import com.ebms.portal.automation.pages.login.ForgotPasswordUsernameEmailPage;
import com.ebms.portal.automation.pages.login.ForgotUsernamePasswordPage;
import io.cucumber.java.en.*;
import org.fest.assertions.api.Assertions;

public class StepsForgotPassword {

    Context context;

    public StepsForgotPassword(Context context) {
        this.context = context;
    }

    @When("The user clicks on forgot password link")
    public void theUserClicksOnForgotPasswordLink() {
        ForgotUsernamePasswordPage forgotUsernamePasswordPage = ObjectLoader.getComponent("Forgot Username Password Page");
        forgotUsernamePasswordPage.getIForgotMyPassword().click();
    }

    @And("The user enters their valid email or username {string}")
    public void theUserEntersTheirValidEmailOrUsername(String username) {
        ForgotPasswordUsernameEmailPage forgotPasswordUsernameEmailPage = ObjectLoader.getComponent("Forgot Password Username Email Page");
        forgotPasswordUsernameEmailPage.getUsernameEmail().setValue(username);

    }

    @And("Clicks on the next button")
    public void clicksOnTheNextButton() {
        ForgotPasswordUsernameEmailPage forgotPasswordUsernameEmailPage = ObjectLoader.getComponent("Forgot Password Username Email Page");
        forgotPasswordUsernameEmailPage.getNext().click();
    }

    @Then("The user selects code option from request code page")
    public void theUserSelectsCodeOptionFromRequestCodePage() {
        ForgotPasswordRequestCodePage forgotPasswordRequestCodePage = ObjectLoader.getComponent("Forgot Password Request Code Page");
        forgotPasswordRequestCodePage.getCodeOption1().click();
    }

    @And("User should be able to click on Request a Verification Code button")
    public void userShouldBeAbleToClickOnRequestAVerificationCodeButton() {
        ForgotPasswordRequestCodePage forgotPasswordRequestCodePage = ObjectLoader.getComponent("Forgot Password Request Code Page");
        Assertions.assertThat(forgotPasswordRequestCodePage.getRequestVerificationCode().isDisplayed() && forgotPasswordRequestCodePage.getRequestVerificationCode().isEnabled());
    }

    @And("The user enters invalid email or username {string}")
    public void theUserEntersInvalidEmailOrUsername(String username) {
        ForgotPasswordUsernameEmailPage forgotPasswordUsernameEmailPage = ObjectLoader.getComponent("Forgot Password Username Email Page");
        forgotPasswordUsernameEmailPage.getUsernameEmail().setValue(username);
    }

    @Then("The user should see an error message {string}")
    public void theUserShouldSeeAnErrorMessage(String message) {
        ForgotPasswordUsernameEmailPage forgotPasswordUsernameEmailPage = ObjectLoader.getComponent("Forgot Password Username Email Page");
        forgotPasswordUsernameEmailPage.getErrorMessage().shouldBe(Condition.visible);
        Assertions.assertThat(message).isEqualTo(forgotPasswordUsernameEmailPage.getErrorMessage().getText());
    }
}
