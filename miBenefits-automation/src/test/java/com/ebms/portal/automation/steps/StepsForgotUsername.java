package com.ebms.portal.automation.steps;

import com.codeborne.selenide.Condition;
import com.ebms.portal.automation.builder.factory.ObjectLoader;
import com.ebms.portal.automation.context.Context;
import com.ebms.portal.automation.pages.login.*;
import io.cucumber.java.en.*;
import org.fest.assertions.api.Assertions;
import io.cucumber.datatable.DataTable;

import java.util.List;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Selenide.*;

public class StepsForgotUsername {
    Context context;

    public StepsForgotUsername(Context context) {
        this.context = context;
    }

    @Given("The user is on login page and clicks on need help link")
    public void theuserisonloginpageandclicksonneedhelplink() {
        LoginPage loginPage = ObjectLoader.getComponent("Login Page");
        assert loginPage != null;
        loginPage.getNeedHelp().click();
    }

    @Then("The user should navigate to forgot username password page$")
    public void theusershouldnavigatetoforgotusernamepasswordpage() {
        ForgotUsernamePasswordPage forgotusernamepasswordpage = ObjectLoader.getComponent("Forgot Username Password Page");
        assert forgotusernamepasswordpage != null;
        Assertions.assertThat(forgotusernamepasswordpage.getBackToLogin().isDisplayed());
    }

    @When("The user clicks on forgot username link")
    public void theUserClicksOnForgotUsernameLink() {
        ForgotUsernamePasswordPage forgotusernamepasswordpage = ObjectLoader.getComponent("Forgot Username Password Page");
        assert forgotusernamepasswordpage != null;
        forgotusernamepasswordpage.getIForgotMyUsername().click();
    }

    @And("Selects {string} from select label field and enters a valid member ID {string}")
    public void selectsFromSelectLabelFieldAndEntersAValidMemberID(String label, String memberid) {
        ForgotUsernameMemberProviderPage forgotusernamememberproviderpage = ObjectLoader.getComponent("Forgot Username Member Provider Page");
        assert forgotusernamememberproviderpage != null;
        forgotusernamememberproviderpage.getMemberProviderSelect().click();
        forgotusernamememberproviderpage.getMemberSelect().click();
        forgotusernamememberproviderpage.getMemberID().setValue(memberid);
    }

    @And("Clicks on next button")
    public void clicksOnNextButton() {
        ForgotUsernameMemberProviderPage forgotusernamememberproviderpage = ObjectLoader.getComponent("Forgot Username Member Provider Page");
        assert forgotusernamememberproviderpage != null;
        forgotusernamememberproviderpage.getNext().click();
    }

    @Then("The user should be able to see email message page")
    public void theUserShouldBeAbleToSeeEmailMessagePage() {
        ForgotUsernameMemberMessagePage forgotUsernameMemberMessagePage = ObjectLoader.getComponent("Forgot Username Member Message Page");
        assert forgotUsernameMemberMessagePage != null;
        Assertions.assertThat(forgotUsernameMemberMessagePage.getMemberMessage().isDisplayed());
    }

    @And("Selects {string} from select label field and enters an invalid member ID {string}")
    public void selectsFromSelectLabelFieldAndEntersAnInvalidMemberID(String label, String memberID) {
        ForgotUsernameMemberProviderPage forgotusernamememberproviderpage = ObjectLoader.getComponent("Forgot Username Member Provider Page");
        assert forgotusernamememberproviderpage != null;
        forgotusernamememberproviderpage.getMemberProviderSelect().click();
        forgotusernamememberproviderpage.getMemberSelect().click();
        forgotusernamememberproviderpage.getMemberID().setValue(memberID);
    }

    @Then("The user should see the error message {string}")
    public void theUserShouldSeeTheErrorMessage(String message) {
        ForgotUsernameMemberProviderPage forgotusernamememberproviderpage = ObjectLoader.getComponent("Forgot Username Member Provider Page");
//        try {
//            Thread.sleep(7000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        assert forgotusernamememberproviderpage != null;
        forgotusernamememberproviderpage.getErrorMessage().shouldBe(Condition.visible);
        Assertions.assertThat(forgotusernamememberproviderpage.getErrorMessage().getText()).isEqualTo(message);
    }

    @And("The user should get an email on the registered email id {string}")
    public void theUserShouldGetAnEmailOnTheRegisteredEmailId(String email) {
        open("https://www.mailinator.com/");
        $("#search").setValue(email).pressEnter();
        String subject = $("#row_buxton-1640589776-133192").getText();
        Assertions.assertThat(subject).contains("miBenefits | Username for Forgot Username Request");
    }

    @Then("The user should be able to see Select Account Pop up message {string}")
    public void theUserShouldBeAbleToSeeSelectAccountPopup(String message) {
        ForgotUsernameSelectAccountPage forgotUsernameSelectAccountPage = ObjectLoader.getComponent("Forgot Username Select Account Page");
        assert forgotUsernameSelectAccountPage != null;
        Assertions.assertThat(forgotUsernameSelectAccountPage.getSelectAccountMessage().getText()).contains(message);
    }

    @Then("The user should be able to see following for accounts information")
    public void theUserShouldBeAbleToSeeFollowingForAccountsInformation(DataTable table) {
        ForgotUsernameSelectAccountPage forgotUsernameSelectAccountPage = ObjectLoader.getComponent("Forgot Username Select Account Page");
        assert forgotUsernameSelectAccountPage != null;
        List<String> expectedElements = table.asList().stream().sorted().collect(Collectors.toList());
        for (String i : expectedElements) {
            Assertions.assertThat(forgotUsernameSelectAccountPage.getAccount1Elements().getText()).contains(i);
            Assertions.assertThat(forgotUsernameSelectAccountPage.getAccount2Elements().getText()).contains(i);
        }
    }

    @And("Clicks on continue button")
    public void clicksOnContinueButton() {
        ForgotUsernameSelectAccountPage forgotUsernameSelectAccountPage = ObjectLoader.getComponent("Forgot Username Select Account Page");
        assert forgotUsernameSelectAccountPage != null;
        forgotUsernameSelectAccountPage.getAccountSelect().click();
    }

    @And("The user leaves member ID field empty")
    public void theUserLeavesMemberIDFieldEmpty() {

    }

    @Then("Next button should not be enabled")
    public void nextButtonShouldNotBeEnabled() {
        ForgotUsernameMemberProviderPage forgotusernamememberproviderpage = ObjectLoader.getComponent("Forgot Username Member Provider Page");
        assert forgotusernamememberproviderpage != null;
        Assertions.assertThat(forgotusernamememberproviderpage.getNext().isEnabled()).isFalse();
    }

    @Then("Next button should be enabled")
    public void nextButtonShouldBeEnabled() {
        ForgotUsernameMemberProviderPage forgotusernamememberproviderpage = ObjectLoader.getComponent("Forgot Username Member Provider Page");
        assert forgotusernamememberproviderpage != null;
        Assertions.assertThat(forgotusernamememberproviderpage.getNext().isEnabled());
    }
}