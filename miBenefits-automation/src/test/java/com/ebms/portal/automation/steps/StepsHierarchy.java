package com.ebms.portal.automation.steps;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.WebDriverRunner;
import com.ebms.portal.automation.builder.factory.ObjectLoader;
import com.ebms.portal.automation.context.Context;
import com.ebms.portal.automation.pages.GroupAdministrationPage;
import com.ebms.portal.automation.pages.ReportsPage;
import com.ebms.portal.automation.pages.dashboard.components.HierarchyFilter;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.testng.Assert;

import static com.codeborne.selenide.Selenide.$;

import java.time.Duration;
import java.util.List;

public class StepsHierarchy {

    Context context;

    public StepsHierarchy(Context context) {
        this.context = context;
    }

    @When("User click on hierarchy filter")
    public void userClickOnHierarchyFilter() {
        HierarchyFilter hierarchyFilter = ObjectLoader.getComponent("Hierarchy Filter");
        hierarchyFilter.getHierarchyFilter().click();
    }

    @Then("Filters should contain the text as {string}")
    public void filtersShouldBeSelectedOnHomepageAs(String arg0) {
        HierarchyFilter hierarchyFilter = ObjectLoader.getComponent("Hierarchy Filter");
        hierarchyFilter.getHierarchyFilter().shouldHave(Condition.text(arg0));
    }

    @Then("Filters should contain the text as {string} and {string}")
    public void filtersShouldContainTheTextAsAnd(String arg0, String arg1) {
        HierarchyFilter hierarchyFilter = ObjectLoader.getComponent("Hierarchy Filter");
        hierarchyFilter.getFirstHierarchyFilter().shouldHave(Condition.text(arg0));
        hierarchyFilter.getSecondHierarchyFilter().shouldHave(Condition.text(arg1));
    }

    @And("User selects below locations")
    public void userSelectsBelowLocations(DataTable table) {
        HierarchyFilter hierarchyFilter = ObjectLoader.getComponent("Hierarchy Filter");
        List<String> locations = table.asList();
        assert hierarchyFilter != null;
        hierarchyFilter.getSelectAllCheckbox().click();
        locations.forEach(hierarchyFilter::selectLocation);
        hierarchyFilter.getApplyButton().click();
    }

    @And("User selects below groups")
    public void userSelectsBelowGroups(DataTable table) {
        HierarchyFilter hierarchyFilter = ObjectLoader.getComponent("Hierarchy Filter");
        List<String> groups = table.asList();
        assert hierarchyFilter != null;
        hierarchyFilter.getSelectAllCheckbox().click();
        groups.forEach(hierarchyFilter::selectGroup);
        hierarchyFilter.getApplyButton().click();
    }

    @And("User enter group as {string}")
    public void userEnterGroupAs(String groupName) {
        HierarchyFilter hierarchyFilter = ObjectLoader.getComponent("Hierarchy Filter");
        hierarchyFilter.getGroupTextbox().setValue(groupName);
    }

    @And("User uncheck checkbox for {string}")
    public void userUncheckCheckboxFor(String checkBoxLabel) {
        HierarchyFilter hierarchyFilter = ObjectLoader.getComponent("Hierarchy Filter");
        hierarchyFilter.uncheckCheckboxByLabel(checkBoxLabel);
    }

    @And("User click on Apply button")
    public void userClickOnApplyButton() {
        HierarchyFilter hierarchyFilter = ObjectLoader.getComponent("Hierarchy Filter");
        hierarchyFilter.getApplyButton().click();
    }

    @Then("User should remain on hierarchy popup")
    public void userShouldRemainOnHierarchyPopup() {
    }

    @When("User click on hierarchy link on Group Administration page")
    public void userClickOnHierarchyLink() {
        GroupAdministrationPage groupAdministrationPage = ObjectLoader.getComponent("Group Administration Page");
        groupAdministrationPage.getHierarchyLink().click();
    }

    @And("User selects location {string} in  hierarchy link")
    public void userSelectsLocationInHierarchyLink(String location) {
        GroupAdministrationPage groupAdministrationPage = ObjectLoader.getComponent("Group Administration Page");
        groupAdministrationPage.selectLocationInHierarchyLink(location);
    }

    @Then("Hierarchy link on Group Admin page should contain the text as {string}")
    public void hierarchyLinkOnGroupAdminPageShouldContainTheTextAs(String textExpected) {
        GroupAdministrationPage groupAdministrationPage = ObjectLoader.getComponent("Group Administration Page");
        String textActual = groupAdministrationPage.getHierarchyLink().getText();
        Assert.assertEquals(textActual, textExpected);
    }


    @When("User click on hierarchy link on reports page")
    public void userClickOnHierarchyLinkOnReportsPage() {
        ReportsPage reportsPage = ObjectLoader.getComponent("Reports Page");
        reportsPage.getHierarchyLink().click();

    }

    @And("User selects group {string} in  hierarchy link")
    public void userSelectsGroupInHierarchyLink(String group) {
        ReportsPage reportsPage = ObjectLoader.getComponent("Reports Page");
        reportsPage.selectGroupInHierarchyLink(group);
    }

    @Then("Hierarchy link on reports page should contain the text as {string}")
    public void hierarchyLinkOnReportsPageShouldContainTheTextAs(String textExpected) {
        ReportsPage reportsPage = ObjectLoader.getComponent("Reports Page");
        String textActual = reportsPage.getHierarchyLink().getText();
        Assert.assertEquals(textActual, textExpected);
    }

    @When("User click on hierarchy filter for {string} and selects {string}")
    public void userClickOnHierarchyFilterForAndSelects(String filter, String value) {
        ReportsPage reportsPage = ObjectLoader.getComponent("Reports Page");
        reportsPage.selectValueInReportFilter(filter,value);
    }

    @Then("Hierarchy link should contain the text as {string}")
    public void hierarchyLinkShouldContainTheTextAs(String value) {
        ReportsPage reportsPage = ObjectLoader.getComponent("Reports Page");
        reportsPage.setReportFilters(reportsPage.getReportFilters());
        Assert.assertTrue(reportsPage.getReportFiltersMap().containsKey(value));
    }
}
