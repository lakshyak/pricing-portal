package com.ebms.portal.automation.steps;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.ebms.portal.automation.builder.factory.ObjectLoader;
import com.ebms.portal.automation.builder.factory.annotation.ElementMeta;
import com.ebms.portal.automation.builder.factory.annotation.WaitForLoad;
import com.ebms.portal.automation.builder.factory.annotation.WebSpecificField;
import com.ebms.portal.automation.context.Context;
import com.ebms.portal.automation.pages.MembershipPage;
import com.ebms.portal.automation.pages.membership.Invite_new_employeePage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class StepsInviteNewEmployee {

    Context context;

    public StepsInviteNewEmployee(Context context) {this.context = context;}


    @Then("The user should be able to open the Invite New Employee form")
    public void theUserShouldBeAbleToOpenTheInviteNewEmployeeForm() {

        MembershipPage membershipPage = ObjectLoader.getComponent("Membership Page");
        assert membershipPage != null;
        if(membershipPage.getBtn_invitenewemployee().isDisplayed())
        membershipPage.getBtn_invitenewemployee().click();

        Invite_new_employeePage invite = ObjectLoader.getComponent("Invite New Employee Page");
      //  if(invite.getParent().isDisplayed())
        assert invite != null;
        if(invite.getFormtitle().isDisplayed())
            System.out.println("Invite New Employee Form is opened");
        else
            System.out.println("------Error Occurred------");

    }

    @And("The user should be able to send an invite to the new employee by filling the details {string}, {string}, {string},{string}, {string}, {string},{string},{string}, {string}")
    public void theUserShouldBeAbleToSendAnInviteToTheNewEmployeeByFillingTheDetails(String fname, String lname, String dob, String doh, String ssn, String email, String loc, String id, String salary) {

        Invite_new_employeePage invite = ObjectLoader.getComponent("Invite New Employee Page");
        assert invite != null;
        invite.fetchinputlist(fname, lname, dob, doh, ssn, email,loc, id, salary);
    }
}
