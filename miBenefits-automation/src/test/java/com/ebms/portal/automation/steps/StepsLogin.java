package com.ebms.portal.automation.steps;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.WebDriverRunner;
import com.ebms.portal.automation.builder.factory.ObjectLoader;
import com.ebms.portal.automation.context.Context;

import com.ebms.portal.automation.pages.login.LoginPage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.fest.assertions.api.Assertions;

import java.util.List;
import java.util.Locale;

public class StepsLogin {

    Context context;

    public StepsLogin(Context context) {
        this.context = context;
    }

    @Given("^The user enters the login details for (.*) and click on login button$")
    public void theUserEntersTheLoginDetailsForUserAndClickOnLoginButton(String persona) {

       LoginPage loginPage = ObjectLoader.getComponent("Login Page");
       loginPage.login(persona);
    }


    @When("The user, (.*) clicks on Login button, without entering any username and password")
    public void theUserPersonaClicksOnLoginButtonWithoutEnteringAnyUsernameAndPassword(String persona) {
        LoginPage loginPage = (LoginPage) context.getComponent();
        loginPage.login(persona);
    }

    @Then("The user should see the following error messages")
    public void theUserShouldSeeTheFollowingErrorMessages(List<String> errorMessages) {
        LoginPage loginPage = (LoginPage) context.getComponent();
        Assertions
                .assertThat(errorMessages)
                .isEqualTo(loginPage.getLoginForm().getValidationErrorMessages());
    }

    @When("^The user (.*) enters the login details and click on login button$")
    public void theUserPersonaEntersTheLoginDetailsAndClickOnLoginButton(String persona) {
        LoginPage loginPage = (LoginPage) context.getComponent();
        loginPage.login(persona);
    }

    @Then("^The user should see the error message (.*) for the (.*)$")
    public void theUserShouldSeeTheErrorMessageErrorMessageForTheField(String message, String validationType) {
        LoginPage loginPage = (LoginPage) context.getComponent();
//        try {
////            Thread.sleep(5000);
////        } catch (InterruptedException e) {
////            e.printStackTrace();
////        }
        Assertions
                .assertThat(message)
                .isEqualTo(loginPage.getLoginForm().getValidationErrorMessage(validationType));
    }

    @Then("^The user should be able to see the respective (.*) dashboard$")
    public void theUserShouldBeAbleToSeeTheRespectivePersonaDashboard(String persona) {
        String currentURL = WebDriverRunner.getWebDriver().getCurrentUrl();
        if(persona.equals("CSR"))
            Assertions.assertThat(currentURL).contains("customerservicerep/dashboard");
        else
            Assertions.assertThat(currentURL).contains(persona.toLowerCase(Locale.ROOT) + "/dashboard");
        //System.out.println(persona + " Validation");
    }
//    @When("The user clicks on Login button")
//    public void theUserClicksOnLoginButton() {
//        loginPage.login(persona);
//    }


//    @When("^The user clicks on Login button$")
//    public void theUserClicksOnLoginButton() {
//        MemberLoginPage loginPage = (MemberLoginPage) context.getComponent();
//        loginPage.getLoginButton().click();
//    }
//
//    @Then("^The user should see the following error messages$")
//    public void theUserShouldSeeTheFollowingErrorMessages(List<String> errorMessages) {
//        MemberLoginPage memberLoginPage = (MemberLoginPage) context.getComponent();
//        Assertions
//                .assertThat(errorMessages)
//                .isEqualTo(memberLoginPage.getLoginForm().getValidationErrorMessages());
//    }
//
//    @When("^The user (.*) enters the login details and click on login button$")
//    public void theUserEntersLoginDetailsAndClickOnLoginButton(String user) {
//        MemberLoginPage memberLoginPage = (MemberLoginPage) context.getComponent();
//        memberLoginPage.login(user);
//    }
//
//    @Then("^The user should see the error message (.*) for the (.*)$")
//    public void theUserShouldSeeTheErrorMessageErrorMessageForTheField(String errorMessage, String field) {
//        MemberLoginPage memberLoginPage = (MemberLoginPage) context.getComponent();
//        Assertions
//                .assertThat(errorMessage)
//                .isEqualTo(memberLoginPage.getLoginForm().getValidationErrorMessage());
//    }
//
//    @When("^The user clicks on the (.*) link on Login page$")
//    public void theUserClicksOnTheSearchProviderLinkOnLoginPage(String searchProvider) {
//        MemberLoginPage memberLoginPage = (MemberLoginPage) context.getComponent();
//        memberLoginPage.getLoginLinks().navigateTo(searchProvider);
//        DataBaseConnection db = new DataBaseConnection("DB1");
//        System.out.println("Connection has been established");
//        System.out.println("Ran query for multiple columns >>>>>>>>" + db.getResultSetsWithHeaders(Queries.SampleQuery,"custom_lob","plan_effective_date","effective_date"));
//        db.closeConnection();
//    }
}
