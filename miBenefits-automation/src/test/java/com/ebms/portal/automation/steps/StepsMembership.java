package com.ebms.portal.automation.steps;

import com.codeborne.selenide.WebDriverRunner;
import com.ebms.portal.automation.builder.factory.ObjectLoader;
import com.ebms.portal.automation.context.Context;

//import com.ebms.portal.automation.pages.login.LoginPage;
//import com.ebms.portal.automation.pages.MainDashboardPage;
//import io.cucumber.java.en.Given;
import com.ebms.portal.automation.pages.MembershipPage;
import io.cucumber.java.en.Then;
import org.openqa.selenium.JavascriptExecutor;

import java.util.concurrent.TimeUnit;

public class StepsMembership {

    Context context;
    public StepsMembership(Context context) {
        this.context = context;

    }

    @Then("^The user should reach on the Membership page if Registration statistics is visible$")
    public void theusershouldreachonthemembershippage(){
        MembershipPage membershipPage = ObjectLoader.getComponent("Membership Page");
        assert membershipPage != null;
        Boolean value = membershipPage.getCensus_chart().isDisplayed();

        if(value)
            System.out.println("Reached Membership Page");
        else
            System.out.println("------Error Occurred------");


    }

    @Then("The user should be able to search using (.*)")
    public void theUserShouldBeAbleToSearchUsingFirstName(String fname) {
        MembershipPage membershipPage = ObjectLoader.getComponent("Membership Page");
        assert membershipPage != null;
        membershipPage.getSearch_member().setValue(fname);


    }

    @Then("The user should be allowed to search using (.*)")
    public void theUserShouldBeAllowedToSearchUsingLastName(String lname) {
        MembershipPage membershipPage = ObjectLoader.getComponent("Membership Page");
        assert membershipPage != null;
        membershipPage.getSearch_member().setValue(lname);

    }

    @Then("The user should be able to search his details using (.*)")
    public void theUserShouldBeAbleToSearchHisDetailsUsingSSN(String ssn) {
        MembershipPage membershipPage = ObjectLoader.getComponent("Membership Page");
        assert membershipPage != null;
        membershipPage.getSearch_member().setValue(ssn);

    }

    @Then("The total number of members will be the sum of Registered Members, Non-Registered Members, Registered Dependents, Non-Registered Dependents")
    public void theTotalNumberOfMembersWillBeTheSumOfRegisteredMembersNonRegisteredMembersRegisteredDependentsNonRegisteredDependents() {

        MembershipPage membershipPage = ObjectLoader.getComponent("Membership Page");
        assert membershipPage != null;

        membershipPage.fetchtotalofstatistics();


    }

    @Then("^Verify that the data which is visible at the Registration Statistics should match with the membership listing on the filters (.*) and (.*)$")
    public void verifyThatTheDataWhichIsVisibleAtTheRegistrationStatisticsShouldMatchWithTheMembershipListingOnTheFiltersRelationshipAndRegistrationStatus(String relationship, String registrationstatus) {
        MembershipPage membershipPage = ObjectLoader.getComponent("Membership Page");
        assert membershipPage != null;
        membershipPage.memberlistingdata_registypefilter(registrationstatus);
        membershipPage.memberlistingdata_relationshipfilter(relationship);
//        membershipPage.getDownload_btn().click();
        WebDriverRunner.getWebDriver().manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
       // WebDriverRunner.getBrowserDownloadsFolder().file("Membership_12_24_2021_04_55_0032_1.csv");
        membershipPage.rowcount();



    }
}
