package com.ebms.portal.automation.steps;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.WebDriverRunner;
import com.ebms.portal.automation.builder.factory.ObjectLoader;
import com.ebms.portal.automation.context.Context;
import com.ebms.portal.automation.pages.privacypolicy.PrivacyPolicyPage;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.fest.assertions.api.Assertions;
import static com.codeborne.selenide.Selenide.*;

public class StepsPrivacyPolicy {
    Context context;

    public StepsPrivacyPolicy(Context context) {
        this.context = context;
    }

    @When("The user click on the privacy policy link")
    public void theUserClickOnThePrivacyPolicyLink() {
//        DashboardPage dashboardPage = ObjectLoader.getComponent("Dashboard Page");
//        dashboardPage.getPrivacyPolicy().scrollIntoView(true).click();
        PrivacyPolicyPage privacyPolicyPage = ObjectLoader.getComponent("Privacy Policy Page");
        privacyPolicyPage.getPrivacyPolicy().shouldBe(Condition.visible);
        privacyPolicyPage.getPrivacyPolicy().scrollIntoView(true).click();
    }

    @Then("The user is directed to the GPA page")
    public void theUserIsDirectedToTheGPAPage() {
        PrivacyPolicyPage privacyPolicyPage = ObjectLoader.getComponent("Privacy Policy Page");
        switchTo().window(1);
        String url = WebDriverRunner.getWebDriver().getCurrentUrl();
//      System.out.println(url);
        Assertions.assertThat(url).contains("https://gpatpa.com/privacy.php");
    }

}
