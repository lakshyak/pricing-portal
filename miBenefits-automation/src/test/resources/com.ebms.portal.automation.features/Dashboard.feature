@epic=DashboardFeatures @Login
Feature: Dashboard Page Features


  Scenario: Verify links present in navigation bar on dashboard
    Given The user enters the login details and clicks on the login button for "Employer"
    Then  Below links should be displayed in navigation bar
      | Group Administration      | <Group Administration> |
      | Operational and Financial | <Reports>              |
      | Stop Loss                 | <Stop Loss>            |
      | Tasks                     | <Tasks>                |
      | Claims                    | <Claims>               |
      | Membership                | <Membership>           |
      | Ongoing Enrollments       | <Ongoing Enrollments>  |
      | Open Enrollments          | <Open Enrollments>     |
      | Appeals                   | <Appeals>              |


  Scenario: Verify tabs and charge types in Financial widget on homepage (GPAM-499, TC-1)
    Given The user enters the login details and clicks on the login button for "Administrator"
    Then Below tabs should be displayed in Financial widget
      | Incurred Date  |
      | Paid Date      |
      | Prev Plan Year |
      | Plan Year      |
      | YTD            |
      | This Month     |
      | Custom         |
      | All Products   |
      | Medical        |
      | Dental         |
      | Vision         |
      | Prescription   |
    And Below charge types should be displayed in Financial Widget
      | Total Charges         |
      | Allowed               |
      | Disallowed            |
      | Member Responsibility |
      | COB Savings           |
      | Plan Paid             |


