@epic=Portal_Login @Login
Feature: Forgot Password

  @Vidi2
  Scenario: Validate that the user is able to get password when user forgets the password.
    Given The user is on login page and clicks on need help link
    Then The user should navigate to forgot username password page
    When The user clicks on forgot password link
    And The user enters their valid email or username "Adams123@mailinator.com"
    And Clicks on the next button
    Then The user selects code option from request code page
    And User should be able to click on Request a Verification Code button

  @Vidi1 @vidisha
  Scenario: Validate that the user gets an error message when enters invalid username/password.
    Given The user is on login page and clicks on need help link
    Then The user should navigate to forgot username password page
    When The user clicks on forgot password link
    And The user enters invalid email or username "xyz"
    And Clicks on the next button
    Then The user should see an error message "User not found"