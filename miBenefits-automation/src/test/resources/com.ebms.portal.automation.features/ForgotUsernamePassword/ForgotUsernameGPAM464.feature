@epic=Portal_Login @Login

Feature: Login feature - Forgot Username
  Coverage :
  1. Positive Forgot Username Testcases with valid credentials
  2. Negative Forgot Username Testcases with invalid credentials
  Note : Tags used for Regression suite and Sanity suite

  Background: User clicks on forgot username link
    Given The user is on login page and clicks on need help link
    Then The user should navigate to forgot username password page
    When The user clicks on forgot username link

  @Regression @Sanity
  Scenario:Validate that if the member is having 1 account then on entering the valid Member ID, the user will be shown a success message
    And Selects "Member" from select label field and enters a valid member ID "982500019"
    And Clicks on next button
    Then The user should be able to see email message page
    And The user should get an email on the registered email id "AJMERIuat@mailinator.com"

  @Regression @Sanity
  Scenario: Validate that the user gets the error if invalid member ID is passed.
    And Selects "Member" from select label field and enters an invalid member ID "ABCDEFGH"
    And Clicks on next button
    Then The user should see the error message "Please enter valid member ID"

  @Regression @Sanity
  Scenario: Validate that if the member is having more than 1 account then on entering the Member ID, the user member will be asked to choose the account
    And Selects "Member" from select label field and enters a valid member ID "982500038"
    And Clicks on next button
    Then The user should be able to see Select Account Pop up message "We found 2 accounts registered with us under provided member ID. Please select account for which you wish to retrieve username."

  @Regression @Sanity
  Scenario: Validate that the member details will be visible for the accounts
    And Selects "Member" from select label field and enters a valid member ID "982500038"
    And Clicks on next button
    Then The user should be able to see following for accounts information
      | Member Name   |
      | Member ID     |
      | Employer      |
      | Zipcode       |
      | Date of birth |
      | Email         |

  @Regression
  Scenario: Validate that on selecting any of the account the user will be redirected to success page.
    And Selects "Member" from select label field and enters an invalid member ID "982500038"
    And Clicks on next button
    And Clicks on continue button
    Then The user should be able to see email message page

  @Regression
  Scenario: Validate that the NEXT button is only enabled after providing the Member ID
    And The user leaves member ID field empty
    Then Next button should not be enabled
    And Selects "Member" from select label field and enters a valid member ID "982500038"
    Then Next button should be enabled
