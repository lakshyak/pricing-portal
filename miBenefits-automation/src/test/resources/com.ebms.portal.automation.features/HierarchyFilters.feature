@epic=HierarchyFilters @Login
Feature: Hierarchy Filter on various pages from various logins -
  a) Admin
  - Select one/all/few locations of an employer
  - Deselect all locations (Incomplete)
  b) Employer
  - Select 1/3 location on home page
  - Use Hierarchy Filter on Group Admin, Reports, Stop Loss, Tasks, Claims, Membership, Ongoing Enrollments pages
  c) Broker
  - Select one/few/all locations of a group on home page
  - Use Hierarchy Filter on Reports, Stop Loss, Claims, Membership, Ongoing Enrollments pages


  Scenario: Validate that admin user is able to select all locations of an employer (GPAM-469, TC-1a)
    Given  The user enters the login details and clicks on the login button for "Administrator"
    When User click on hierarchy filter
    And User selects below groups
      | The Townsend Corporation - 00614 |
    Then Filters should contain the text as "EBMS > The Townsend Corporation - 00614" and "+All Locations"


  Scenario: Validate that admin user is able to select few locations of an employer (GPAM-469, TC-1b)
    Given  The user enters the login details and clicks on the login button for "Administrator"
    When User click on hierarchy filter
    And User enter group as "The Townsend Corporation - 00614"
    And User selects below locations
      | Townsend Tree Service |
      | Kelley Electric       |
      | Cobra                 |
    Then Filters should contain the text as "EBMS > The Townsend Corporation - 00614 > Cobra" and "+2 more"

#Incomplete
  Scenario: Validate that admin user is able to deselect all locations (GPAM-469, TC-1c)
    Given  The user enters the login details and clicks on the login button for "Administrator"
    When User click on hierarchy filter
    And User uncheck checkbox for "Select All"
    And User click on Apply button
    Then User should remain on hierarchy popup


  Scenario: Validate that admin user is able to select one location of a group (GPAM-469, TC-1d)
    Given  The user enters the login details and clicks on the login button for "Administrator"
    When User click on hierarchy filter
    And User enter group as "The Townsend Corporation - 00614"
    And User selects below locations
      | Townsend Tree Service |
    Then Filters should contain the text as "EBMS > The Townsend Corporation - 00614 > Townsend Tree Service" and "+All Departments"


  Scenario: Validate that employer user is able to select 1 location in hierarchy filter on home page (GPAM-469, TC-2c)
    Given The user enters the login details and clicks on the login button for "Employer"
    When  User click on hierarchy filter
    And   User selects below locations
      | Townsend Tree Service |
    Then  Filters should contain the text as "The Townsend Corporation - 00614 > Townsend Tree Service" and "+All Departments"


  Scenario: Validate that employer user is able to select 3 locations in hierarchy filter on home page (GPAM-469,TC-2b)
    Given The user enters the login details and clicks on the login button for "Employer"
    When  User click on hierarchy filter
    And   User selects below locations
      | Townsend Tree Service |
      | Kelley Electric       |
      | Cobra                 |
    Then Filters should contain the text as "The Townsend Corporation - 00614 > Cobra" and "+2 more"


  Scenario: Validate that broker user is able to select all locations of a group on home page (GPAM-469, TC-3a)
    Given The user enters the login details and clicks on the login button for "Broker"
    When User click on hierarchy filter
    And User selects below groups
      | Waggoners Trucking - 00124 |
    Then Filters should contain the text as "Waggoners Trucking - 00124" and "+All Locations"


  Scenario: Validate that broker user is able to select few locations of a group on home page (GPAM-469, TC-3b)
    Given The user enters the login details and clicks on the login button for "Broker"
    When User click on hierarchy filter
    And User selects below locations
      | Billings |
      | Bowman   |
      | Greer    |
    Then Filters should contain the text as "Waggoners Trucking - 00124 > Billings" and "+2 more"


    #Incomplete 3c



  Scenario: Validate that broker user is able to select one location of a group on home page (GPAM-469, TC-3d)
    Given The user enters the login details and clicks on the login button for "Broker"
    When User click on hierarchy filter
    And User selects below locations
      | Billings |
    Then Filters should contain the text as "Waggoners Trucking - 00124 > Billings" and "+All Departments"


  Scenario: Validate that employer user is able to use hierarchy filter on Group Administration page (GPAM-469,TC-10)
    Given The user enters the login details and clicks on the login button for "Employer"
    And User should navigate to page "Group Administration"
    When User click on hierarchy link on Group Administration page
    And User selects location "Eco-Pak" in  hierarchy link
    Then Hierarchy link on Group Admin page should contain the text as "The Townsend Corporation - 00614 > Eco-Pak"


  Scenario: Validate that employer user is able to use hierarchy filter on Reports page (GPAM-469,TC-11)
    Given The user enters the login details and clicks on the login button for "Employer"
    And The user should click on "Reports" link and navigate to "Operational and Financial"
    When User click on hierarchy filter for "December" and selects "Year"
    Then  Hierarchy link should contain the text as "Year"

  @Test
  Scenario Outline: Validate that employer user is able to use hierarchy filter on
  Stop Loss, Tasks, Claims, Membership pages (GPAM-469,TC-12, 13, 14, 15)
    Given The user enters the login details and clicks on the login button for "Employer"
    And User should navigate to page <Page>
    When User click on hierarchy filter
    And User selects below locations
      | Townsend Tree Service |
    Then  Filters should contain the text as "The Townsend Corporation - 00614 > Townsend Tree Service" and "+All Departments"
    Examples:
      | Page       |
      | Stop Loss  |
      | Tasks      |
      | Claims     |
      | Membership |


  Scenario: Validate that employer user is able to use hierarchy filter on Ongoing Enrollments page (GPAM-469,TC-16)
    Given The user enters the login details and clicks on the login button for "Employer"
    And The user should click on "Enrollments" link and navigate to "Ongoing Enrollments"
    When User click on hierarchy filter
    And User selects below locations
      | Townsend Tree Service |
    Then  Filters should contain the text as "The Townsend Corporation - 00614 > Townsend Tree Service" and "+All Departments"


  Scenario: Validate that broker user is able to use hierarchy filter on Reports page (GPAM-469,TC-17)
    Given The user enters the login details and clicks on the login button for "Broker"
    And User should navigate to page "Reports"
    When User click on hierarchy link on reports page
    And User selects group "Waggoners Trucking - 00124" in  hierarchy link
    Then Hierarchy link on reports page should contain the text as "Waggoners Trucking - 00124"


  Scenario Outline: Validate that broker user is able to use hierarchy filter on
  Stop Loss, Claims, Membership pages (GPAM-469,TC-18, 19, 20)
    Given The user enters the login details and clicks on the login button for "Broker"
    And User should navigate to page <Page>
    When User click on hierarchy filter
    And User selects below locations
      | Billings |
    Then  Filters should contain the text as "Waggoners Trucking - 00124 > Billings" and "+All Departments"
    Examples:
      | Page       |
      | Stop Loss  |
      | Claims     |
      | Membership |


  Scenario: Validate that broker user is able to use hierarchy filter on Ongoing Enrollments page (GPAM-469,TC-21)
    Given The user enters the login details and clicks on the login button for "Broker"
    And The user should click on "Enrollments" link and navigate to "Ongoing Enrollments"
    When User click on hierarchy filter
    And User selects below locations
      | Billings |
    Then  Filters should contain the text as "Waggoners Trucking - 00124 > Billings" and "+All Departments"