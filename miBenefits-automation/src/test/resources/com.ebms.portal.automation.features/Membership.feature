@epic=Portal_Login @Login
Feature: Membership Page features

  @Sanity @P2 @GPAM-468 @Step-123
  Scenario Outline: Validate that the user is able to navigate to the Membership page
    Given The user enters the login details for <persona> and click on login button
    When The user clicks on Membership button on dashboard
    Then The user should reach on the Membership page if Registration statistics is visible
    Examples:
      | persona  |
      | Admin    |
      | Employer |
      | Broker   |


  @Sanity @P5 @GPAM-469 @Step9
  Scenario Outline: Validate that the user is able to navigate to the Membership page
    Given The user enters the login details for <persona> and click on login button
    When The user clicks on Membership button on dashboard
    Then The total number of members will be the sum of Registered Members, Non-Registered Members, Registered Dependents, Non-Registered Dependents
    Examples:
      | persona  |
      | Admin    |
      | Employer |
      | Broker   |


  @Sanity @P6
  Scenario Outline: Validate that the user is able to perform search on the Membership page using first name
    Given The user enters the login details and clicks on the login button for "Admin"
    When The user clicks on Membership button on dashboard
    Then The user should be able to search using <first name>
    Examples:
      | first name |
      | JADEN      |
      | STEVEN     |


  @Sanity @P6
  Scenario Outline: Validate that the user is able to perform search on the Membership page using last name
    Given The user enters the login details and clicks on the login button for "Admin"
    When The user clicks on Membership button on dashboard
    Then The user should be allowed to search using <last name>
    Examples:
      | last name |
      | Acord     |
      | Abbey     |

  @Sanity @P6
  Scenario Outline: Validate that the user is able to perform search on the Membership page using SSN
    Given The user enters the login details and clicks on the login button for "Admin"
    When The user clicks on Membership button on dashboard
    Then The user should be able to search his details using <SSN>
    Examples:
      | SSN         |
      | 516-11-3211 |
      | 474-04-4014 |

  @Sanity @P8
  Scenario: Validate that the user is able to send an invite to new employer
    Given The user enters the login details and clicks on the login button for "Admin"
    When The user clicks on Membership button on dashboard
    Then The user should be able to open the Invite New Employee form
    And The user should be able to send an invite to the new employee by filling the details "Ankita", "Middha", "02-02-1989","02-02-2019", "872-99-0147", "tushar@mailinator.com","GPA","1320", "600000"


  @Sanity @P2 @GPAM-468 @Step-1011
  Scenario Outline: Validate that the user is able to navigate to the Membership page
    Given The user enters the login details for <persona> and click on login button
    When The user clicks on Membership button on dashboard
    Then Verify that the data which is visible at the Registration Statistics should match with the membership listing on the filters <Relationship> and <Registration Status>
    Examples:
      | Relationship  | Registration Status | persona  |
#      | Policy Holder | Registered          | Admin    |
#     | Policy Holder | Non-Registered      | Employer |
      | Policy Holder | Registered      | Employer |
#      | All           | Registered          | Broker   |
#      |All            | Non-Registered      | Admin    |
