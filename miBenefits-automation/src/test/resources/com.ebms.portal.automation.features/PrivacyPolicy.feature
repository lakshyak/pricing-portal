Feature: Privacy Policy

  @privacy
  Scenario Outline: Verify that at the footer of Dashboard Page : Privacy policy link is redirecting to the GPA Page
    Given The user enters the login details for <persona> and click on login button
    When The user click on the privacy policy link
    Then The user is directed to the GPA page
    Examples:
      | persona       |
      | Administrator |
      | Employer      |
      | Member        |
      | Broker        |