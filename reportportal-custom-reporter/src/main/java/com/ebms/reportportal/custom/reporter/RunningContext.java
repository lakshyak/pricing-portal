package com.ebms.reportportal.custom.reporter;


import com.epam.ta.reportportal.ws.model.attribute.ItemAttributesRQ;
import cucumber.api.PickleStepTestStep;
import cucumber.api.TestCase;
import cucumber.api.TestStep;
import cucumber.api.event.TestSourceRead;
import gherkin.AstBuilder;
import gherkin.Parser;
import gherkin.ParserException;
import gherkin.TokenMatcher;
import gherkin.ast.*;
import gherkin.pickles.PickleTag;
import io.reactivex.Maybe;

import java.util.*;

class RunningContext {
    private RunningContext() {
        throw new AssertionError("No instances should exist for the class!");
    }

    static class ScenarioContext {
        private static final Map<String, String> outlineIterationsMap = new HashMap<>();
        private Maybe<String> id = null;
        private Background background;
        private ScenarioDefinition scenario;
        private final Queue<Step> backgroundSteps = new ArrayDeque<>();
        private final Map<Integer, Step> scenarioLocationMap = new HashMap<>();
        private Set<ItemAttributesRQ> attributes = new HashSet<>();
        private TestCase testCase;
        private boolean hasBackground = false;
        private String scenarioDesignation;

        ScenarioContext() {}

        void processScenario(ScenarioDefinition scenario) {
            this.scenario = scenario;
            for (Step step : scenario.getSteps()) {
                this.scenarioLocationMap.put(step.getLocation().getLine(), step);
            }
        }

        void processBackground(Background background) {
            if (background != null) {
                this.background = background;
                this.hasBackground = true;
                this.backgroundSteps.addAll(background.getSteps());
                this.mapBackgroundSteps(background);
            }
        }

        public Set<ItemAttributesRQ> getAttributes() {
            return this.attributes;
        }

        void processScenarioOutline(ScenarioDefinition scenarioDefinition) {
            if (this.isScenarioOutline(scenarioDefinition) && !this.hasOutlineSteps()) {
                String outlineIdentifyer = " [" + this.scenarioDesignation.replaceAll(".*\\.feature:| #.*", "") + "]";
                outlineIterationsMap.put(this.scenarioDesignation, outlineIdentifyer);
            }
        }

        void processTags(List<PickleTag> pickleTags) {
            this.attributes = Utils.extractPickleTags(pickleTags);
        }

        void mapBackgroundSteps(Background background) {
            for (Step step : background.getSteps()) {
                this.scenarioLocationMap.put(step.getLocation().getLine(), step);
            }
        }

        String getName() {
            return this.scenario.getName();
        }

        String getKeyword() {
            return this.scenario.getKeyword();
        }

        int getLine() {
            return this.isScenarioOutline(this.scenario) ? this.testCase.getLine() : this.scenario.getLocation().getLine();
        }

        String getStepPrefix() {
            return this.hasBackground() && this.withBackground() ? this.background.getKeyword().toUpperCase() + ": " : "";
        }

        Step getStep(TestStep testStep) {
            PickleStepTestStep pickleStepTestStep = (PickleStepTestStep)testStep;
            Step step = this.scenarioLocationMap.get(pickleStepTestStep.getStepLine());
            if (step != null) {
                return step;
            } else {
                throw new IllegalStateException(String.format("Trying to get step for unknown line in feature. Scenario: %s, line: %s", this.scenario.getName(), this.getLine()));
            }
        }

        Maybe<String> getId() {
            return this.id;
        }

        void setId(Maybe<String> newId) {
            if (this.id != null) {
                throw new IllegalStateException("Attempting re-set scenario ID for unfinished scenario.");
            } else {
                this.id = newId;
            }
        }

        void setTestCase(TestCase testCase) {
            this.testCase = testCase;
            this.scenarioDesignation = testCase.getScenarioDesignation();
        }

        void nextBackgroundStep() {
            this.backgroundSteps.poll();
        }

        boolean isScenarioOutline(ScenarioDefinition scenario) {
            return scenario instanceof ScenarioOutline;
        }

        boolean withBackground() {
            return !this.backgroundSteps.isEmpty();
        }

        boolean hasBackground() {
            return this.hasBackground && this.background != null;
        }

        boolean hasOutlineSteps() {
            return outlineIterationsMap.get(this.scenarioDesignation) != null && !outlineIterationsMap.get(this.scenarioDesignation).isEmpty();
        }

        String getOutlineIteration() {
            return this.hasOutlineSteps() ? outlineIterationsMap.get(this.scenarioDesignation) : null;
        }
    }

    static class FeatureContext {
        private static final Map<String, TestSourceRead> pathToReadEventMap = new HashMap<>();
        private String currentFeatureUri;
        private Maybe<String> currentFeatureId;
        private Feature currentFeature;
        private Set<ItemAttributesRQ> attributes = new HashSet<>();

        FeatureContext() {}

        static void addTestSourceReadEvent(String path, TestSourceRead event) {
            pathToReadEventMap.put(path, event);
        }

            ScenarioContext getScenarioContext(TestCase testCase) {
            ScenarioDefinition scenario = this.getScenario(testCase);
            ScenarioContext context = new ScenarioContext();
            context.processScenario(scenario);
            context.setTestCase(testCase);
            context.processBackground(this.getBackground());
            context.processScenarioOutline(scenario);
            context.processTags(testCase.getTags());
            return context;
        }

            FeatureContext processTestSourceReadEvent(TestCase testCase) {
            TestSourceRead event = pathToReadEventMap.get(testCase.getUri());
            this.currentFeature = this.getFeature(event.source);
            this.currentFeatureUri = event.uri;
            this.attributes = Utils.extractAttributes(this.currentFeature.getTags());
            return this;
        }

        Feature getFeature(String source) {
            Parser<GherkinDocument> parser = new Parser<>(new AstBuilder());
            TokenMatcher matcher = new TokenMatcher();

            GherkinDocument gherkinDocument;
            try {
                gherkinDocument = parser.parse(source, matcher);
            } catch (ParserException var6) {
                return null;
            }

            return gherkinDocument.getFeature();
        }

        Background getBackground() {
            ScenarioDefinition background = this.getFeature().getChildren().get(0);
            return background instanceof Background ? (Background)background : null;
        }

        Feature getFeature() {
            return this.currentFeature;
        }

        Set<ItemAttributesRQ> getAttributes() {
            return this.attributes;
        }

        String getUri() {
            return this.currentFeatureUri;
        }

        Maybe<String> getFeatureId() {
            return this.currentFeatureId;
        }

        void setFeatureId(Maybe<String> featureId) {
            this.currentFeatureId = featureId;
        }

        <T extends ScenarioDefinition> T getScenario(TestCase testCase) {
            List<ScenarioDefinition> featureScenarios = this.getFeature().getChildren();
            Iterator<ScenarioDefinition> var3 = featureScenarios.iterator();

            while(true) {
                ScenarioDefinition scenario;
                do {
                    do {
                        if (!var3.hasNext()) {
                            throw new IllegalStateException("Scenario can't be null!");
                        }
                        scenario = var3.next();
                    } while(scenario instanceof Background);
                    if (testCase.getLine() == scenario.getLocation().getLine() && testCase.getName().equals(scenario.getName())) {
                        return (T) scenario;
                    }
                } while(!(scenario instanceof ScenarioOutline));

                for (Examples example : ((ScenarioOutline) scenario).getExamples()) {
                    for (TableRow tableRow : example.getTableBody()) {
                        if (tableRow.getLocation().getLine() == testCase.getLine()) {
                            return (T) scenario;
                        }
                    }
                }
            }
        }
    }
}
